Pod::Spec.new do |s|

  s.name         = "MESChartView"
  s.version      = "0.7.4"
  s.summary      = "MESChartView is a stock chart engine."

  s.homepage     = "http://moex.com"
  s.license      = { :type => 'GPL Version 3', :file => 'LICENSE' }
  s.author       = { "denn nevera (book pro)" => "denn.nevera@gmail.com" }

  s.platform     = :ios, '8.0'
  s.ios.deployment_target = '8.0'

  s.source       = { :git => "https://bitbucket.org/denn_nevera/meschartview.git", :tag => s.version }
  s.source_files  = 'MESChartView/Classes', 'Classes/**/*.{h,m}'
  s.public_header_files = 'MESChartView/Classes/**/*.h'

  s.frameworks = 'Foundation', 'CoreGraphics'
  s.requires_arc = true

end
