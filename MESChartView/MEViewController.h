//
//  MEViewController.h
//  MESChartView
//
//  Created by denis svinarchuk on 2/25/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MESChartView.h"

@interface MEViewController : UIViewController
@property(nonatomic,strong) MESChartView *chart;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@end
