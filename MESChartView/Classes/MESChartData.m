//
//  MESChartData.m
//  MESChartView
//
//  Created by denis svinarchuk on 2/25/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartData.h"

@implementation MESChartData
- (NSString*) description{
    return [NSString stringWithFormat:@"schart data[%li]: %@:%@", (long)_index, _xValue, _yValue];
}

@synthesize yValue = _yValue;

- (BOOL) isEqualxValue:(id)object{
    return [self.xValue isEqual:[object xValue]];
}

- (BOOL) isEqualyValue:(id)object{
    return [self.yValue isEqual:[object yValue]];
}


- (BOOL) isEqual:(id)object{
    return [self isEqualxValue:object] && [self isEqualyValue:object];
}

- (void) setYValue:(id)yValue{
    if ([yValue isKindOfClass:[NSArray class]]) {
        _yValue = [yValue objectAtIndex:0];
    }
    else
        _yValue = yValue;
}

@end

@implementation MESChartDataRange

- (id) initWithMinimum:(id)min withMaximum:(id)max{
    
    self = [super init];
    
    if (self) {        
        _minValue = min;
        _maxValue = max;
    }
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"schart range: %@:%@", _minValue, _maxValue];
}
@end


@implementation MESChartOHLC
- (NSString*) description{
    return [NSString stringWithFormat:@"%@:%@:%@:%@", _open, _high, _low, _close];            
}

- (id) initWithScalarOpen:(CGFloat)open withHigh:(CGFloat)high withLow:(CGFloat)low withClose:(CGFloat)close{
    self = [super init];
    
    if (self) {
        _open = [NSNumber numberWithFloat:open];    
        _high = [NSNumber numberWithFloat:high];    
        _low = [NSNumber numberWithFloat:low];    
        _close = [NSNumber numberWithFloat:close];
    }
        
    return self;
}

- (id) initWithOpen:(NSNumber *)open withHigh:(NSNumber *)high withLow:(NSNumber *)low withClose:(NSNumber *)close{
    self = [super init];
    
    if (self) {
        _open = open;    
        _high = high;    
        _low = low;    
        _close = close;
    }
    
    return self;
}

- (MESChartCandelType) candelType{
    return _close>_open?MESCHART_CANDLE_GROUTH:_close<_open?MESCHART_CANDLE_FALLING:MESCHART_CANDLE_FLAT;
}

- (CGFloat) floatValue{
    return [_close floatValue];
}

- (NSInteger) integerValue{
    return (NSInteger) roundf([_close floatValue]);
}

- (double) doubleValue{
    return [_close doubleValue];
}

@end