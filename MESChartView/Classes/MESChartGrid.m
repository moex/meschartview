//
//  MESChartGrid.m
//  MESChartView
//
//  Created by denis svinarchuk on 2/26/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartGrid.h"

CGFloat static const kChartGridLineWidth = .3f;
#define kChartGridColor [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]

@interface MESChartGrid()
@end

@implementation MESChartGrid

@synthesize xAxis=_xAxis, yAxis=_yAxis, xLayer=_xLayer;

- (id) init{
    self = [super init];
    if (self) {                    
        self.lineWidth = kChartGridLineWidth;
        self.enableGradient = NO;
        self.strokeColor = kChartGridColor.CGColor;
        self.backgroundColor = [UIColor clearColor].CGColor;
        self.lineDashPattern = @[@1,@2];
    }
    return self;
}

- (CAShapeLayer*) yLayer{
    return self;
}

- (CAShapeLayer*) xLayer{
    if (_xLayer) {
        return _xLayer;
    }
    
    _xLayer = [CAShapeLayer layer];
    _xLayer.lineWidth = self.lineWidth;
    _xLayer.strokeColor = self.strokeColor;
    _xLayer.backgroundColor = [UIColor clearColor].CGColor;
    _xLayer.lineDashPattern = nil;
    
    [self addSublayer:_xLayer];
    
    return _xLayer;
}

- (MESChartAxis*) xAxis{
    if (_xAxis) {
        return _xAxis;
    }
    _xAxis = [[MESChartAxis alloc] init];
    return _xAxis;
}

- (MESChartAxis*) yAxis{
    if (_yAxis) {
        return _yAxis;
    }
    
    _yAxis = [[MESChartAxis alloc] init];    
    
    return _yAxis;
}

@end
