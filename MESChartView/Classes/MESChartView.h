//
//  MESChartView.h
//  MESChartView
//
//  Created by denis svinarchuk on 2/25/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MESChartData.h"
#import "MESChartGrid.h"
#import "MESChartSeries.h"
#import "MESChartCandlesCanvas.h"
#import "MESChartProtocols.h"

@interface MESChartTitleView: UIView
@property (nonatomic,readonly) UILabel *textLabel;
@property (nonatomic,readonly) UILabel *priceLabel;
@end

/**
 *  
 */
@interface MESChartView : UIView
@property (nonatomic,readonly) MESChartTitleView *titleView;
@property (nonatomic,readonly) UIView *headerView;

@property (nonatomic,readonly) CALayer *rootLayer;
@property (nonatomic,readonly) MESChartCanvas *graphLayer;
@property (nonatomic,readonly) MESChartGrid *gridLayer;
@property (nonatomic,readonly) MESChartCandlesCanvas *candlesLayer;
@property (nonatomic,readonly) CAShapeLayer *crossHairLayer;
@property (nonatomic,readonly) MESChartSelectionLayer *selectionLayer;

@property (nonatomic,assign) CGFloat  edgePadding;
@property (nonatomic,assign) CGFloat  zonePadding;
@property (nonatomic,strong) UIImage *backgroundImage;

@property (nonatomic,strong) id<MESChartDataSource> dataSource;
@property (nonatomic,strong) id<MESChartFormater> formater;
@property (nonatomic,strong) id<MESChartDelegate> delegate;

@property (nonatomic,assign) BOOL enableRedrawAnimation;

- (void) redraw;
- (UIImage *)screenShot;
@end
