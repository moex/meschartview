//
//  MESChartView.m
//  MESChartView
//
//  Created by denis svinarchuk on 2/25/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartView.h"
#import "MESChartCanvas.h"
#import "MESChartCandlesCanvas.h"

@interface NSDate(MEChartComparing)
- (NSDate *) dateOnly;
@end

@implementation NSDate(MEChartComparing)

- (NSDate *) dateOnly{
    NSDateComponents *components = [[NSCalendar currentCalendar]
                                    components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                    fromDate:self];
    return [[NSCalendar currentCalendar]
            dateFromComponents:components];
}

@end

CGFloat static const kChartLineViewEdgePadding = 10.0f;
CGFloat static const kChartZoneHeight = 50.0f;

@interface __MESChartPaths : NSObject
@property (nonatomic,readonly) CGMutablePathRef flatPath;
@property (nonatomic,readonly) CGMutablePathRef grouthPath;
@property (nonatomic,readonly) CGMutablePathRef fallingPath;
@property (nonatomic,readonly) CGMutablePathRef overMidnightPath;
@end

@implementation __MESChartPaths

- (id) init{
    self = [super init];
    
    if (self) {
        _grouthPath = CGPathCreateMutable();
        _fallingPath = CGPathCreateMutable();
        _overMidnightPath = CGPathCreateMutable();
    }
    
    return self;
};


- (CGMutablePathRef) flatPath{
    return _grouthPath;
}


- (void) dealloc{
    CGPathRelease(_grouthPath);
    CGPathRelease(_fallingPath);
    CGPathRelease(_overMidnightPath);
}

@end

@interface __MESChartGridPaths : NSObject
@property (nonatomic,readonly) CGMutablePathRef yPath;
@property (nonatomic,readonly) CGMutablePathRef xPath;
@end

@implementation __MESChartGridPaths

- (id) init{
    self = [super init];
    
    if (self) {
        _xPath = CGPathCreateMutable();
        _yPath = CGPathCreateMutable();
    }
    
    return self;
};


- (void) dealloc{
    CGPathRelease(_yPath);
    CGPathRelease(_xPath);
}

@end

@interface MESChartTitleView()
@property (nonatomic,strong) UILabel *textLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@end

@implementation MESChartTitleView

- (UILabel*) textLabel{
    if (_textLabel) {
        return _textLabel;
    }
    
    _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
    [self addSubview:_textLabel];
    
    _textLabel.translatesAutoresizingMaskIntoConstraints=NO;
    
    NSNumber *labelPadding =  [NSNumber numberWithFloat:10];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[label]-(padding)-[price]" options:0 metrics:@{@"padding": labelPadding} views:@{@"label": _textLabel, @"price": self.priceLabel}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(padding)-[label]-(padding)-|" options:0 metrics:@{@"padding": labelPadding} views:@{@"label": _textLabel}]];
    
    _textLabel.textColor = [UIColor blackColor];
    _textLabel.textAlignment = NSTextAlignmentCenter;
    _textLabel.layer.allowsEdgeAntialiasing = YES;
    _textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:self.bounds.size.height/2.];
    
    _textLabel.backgroundColor = [UIColor clearColor];
    
    return _textLabel;
}


- (UILabel*) priceLabel{
    if (_priceLabel) {
        return _priceLabel;
    }
    
    _priceLabel = [[UILabel alloc] initWithFrame:self.bounds];
    [self addSubview:_priceLabel];
    
    _priceLabel.translatesAutoresizingMaskIntoConstraints=NO;
    
    NSNumber *labelPadding =  [NSNumber numberWithFloat:10];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[price]-(padding)-|" options:0 metrics:@{@"padding": labelPadding} views:@{@"price": self.priceLabel}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(padding)-[price]-(padding)-|" options:0 metrics:@{@"padding": labelPadding} views:@{@"price": self.priceLabel}]];
    
    _priceLabel.textColor = [UIColor blackColor];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.layer.allowsEdgeAntialiasing = YES;
    _priceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:self.bounds.size.height/2.];
    
    _priceLabel.backgroundColor = [UIColor clearColor];
    
    return _priceLabel;
}


@end

@interface __MESChartHeaderLabel : UILabel
@end

@implementation __MESChartHeaderLabel

- (void) setBackgroundColor:(UIColor *)backgroundColor{
    self.superview.backgroundColor = backgroundColor;
    [super setBackgroundColor: [UIColor clearColor]];
}

- (UIColor*) backgroundColor{
    return self.superview.backgroundColor;
}

@end


@interface MESChartView() <UIGestureRecognizerDelegate>
@property (nonatomic,strong) MESChartTitleView      *titleView;
@property (nonatomic,strong) UIView                 *headerView;
@property (atomic, assign  ) BOOL                   headerInProgress;
@property (nonatomic,strong) __MESChartHeaderLabel  *headerValueLabel;

@property (nonatomic,strong) CALayer                *rootLayer;
@property (nonatomic,assign) CGFloat                mainGrapthLayerHeight;

@property (nonatomic,strong) MESChartCanvas         *graphLayer;// main series canvas
@property (nonatomic,strong) CALayer                *backgroundImageLayer;

@property (nonatomic,strong) CAShapeLayer           *crossHairLayer;
@property (nonatomic,strong) CAShapeLayer           *crossHairDecorationLayer;
@property (nonatomic,strong) MESChartSelectionLayer *selectionLayer;

@property (nonatomic,strong) MESChartCandlesCanvas  *candlesLayer;

@property (nonatomic,strong) MESChartGrid           *gridLayer;
@property (nonatomic,assign) CGFloat                oneNodeWidth;
@property (nonatomic,strong) NSMutableArray         *graphZones;
@property (nonatomic,strong) NSMutableArray         *gridZones;


@end

@implementation MESChartView
{
    CGFloat barWidth;
    CGPoint lastTouched;
    CGPoint lastTouched2;
    
    UIPanGestureRecognizer   *panGesture;
    UIPinchGestureRecognizer *pinchGesture;
    
    BOOL isTouched;
    
    NSSet *lastTouchesBegan;
}

@synthesize graphLayer=_graphLayer, gridLayer=_gridLayer, rootLayer=_rootLayer;


+ (void) setAnimationDuration:(NSTimeInterval)duration{
    [super setAnimationDuration:duration];
    [MESChartCanvas setAnimationDuration:duration];
}


#pragma mark - Defaults

- (void) setup{
    
    _enableRedrawAnimation = YES;
    _edgePadding = _zonePadding = -1;
    _oneNodeWidth = 1.0;
    _mainGrapthLayerHeight = -1.;
    self.layer.drawsAsynchronously = YES;
    barWidth = 10.;
    lastTouched = CGPointMake(-barWidth-1., -1.);
    self.multipleTouchEnabled = YES;
    self.exclusiveTouch = YES;
    
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:nil];
    panGesture.minimumNumberOfTouches = 1;
    panGesture.maximumNumberOfTouches = 1;
    panGesture.delegate = self;
    panGesture.enabled = NO;
    
    [self addGestureRecognizer:panGesture];
    
    pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchHandler:)];
    pinchGesture.delegate = self;
    pinchGesture.delaysTouchesBegan = NO;
    pinchGesture.delaysTouchesEnded = YES;
    
    [self addGestureRecognizer:pinchGesture];
}


#pragma mark - Constructors

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (id) init{
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

#pragma mark - Properties


- (CGRect) headerFrame{
    CGRect rect = CGRectMake(0, 0, self.bounds.size.width, 36);
    return rect;
}

- (UIView*) headerView{
    if (_headerView) {
        return _headerView;
    }
    
    _headerView = [[UIView alloc] initWithFrame:[self headerFrame]];
    _headerView.layer.drawsAsynchronously = YES;
    _headerView.alpha = 0.0;
    _headerView.layer.borderWidth = self.gridLayer.yAxis.labelPadding;
    _headerView.layer.borderColor = self.backgroundColor.CGColor;
    _headerView.layer.masksToBounds = YES;
    _headerView.layer.cornerRadius = (_headerView.bounds.size.height)/2.;
    
    [self addSubview:_headerView];
    
    return _headerView;
}

- (CGRect) titleFrame{
    return CGRectMake(0, 0, self.bounds.size.width, self.headerView.bounds.size.height);
}

- (UIView*) titleView{
    if (_titleView) {
        return _titleView;
    }
    CGRect rect = [self titleFrame];
    
    _titleView = [[MESChartTitleView alloc] initWithFrame:rect];
    _titleView.layer.drawsAsynchronously = YES;
    _titleView.backgroundColor = [UIColor clearColor];
    _titleView.alpha = 1.0;
    
    [self insertSubview:_titleView belowSubview:self.headerView];
    
    return _titleView;
}


- (UILabel*) headerValueLabel{
    
    if (_headerValueLabel) {
        return _headerValueLabel;
    }
    
    _headerValueLabel = [[__MESChartHeaderLabel alloc] initWithFrame:_headerView.bounds];
    [self.headerView addSubview:_headerValueLabel];
    
    _headerValueLabel.translatesAutoresizingMaskIntoConstraints=NO;
    
    NSNumber *vlabelPadding =  [NSNumber numberWithFloat:self.gridLayer.yAxis.labelPadding];
    NSNumber *hlabelPadding =  [NSNumber numberWithFloat:self.gridLayer.yAxis.labelPadding+_headerValueLabel.bounds.size.height/4.];
    
    [_headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[label]-(padding)-|" options:0 metrics:@{@"padding": hlabelPadding} views:@{@"label": _headerValueLabel}]];
    [_headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(padding)-[label]-(padding)-|" options:0 metrics:@{@"padding": vlabelPadding} views:@{@"label": _headerValueLabel}]];
    
    _headerValueLabel.textColor = [UIColor whiteColor];
    _headerValueLabel.textAlignment = NSTextAlignmentCenter;
    _headerValueLabel.layer.allowsEdgeAntialiasing = YES;
    
    _headerValueLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:_headerValueLabel.bounds.size.height/2.];
    _headerValueLabel.adjustsFontSizeToFitWidth = YES;
    _headerValueLabel.minimumScaleFactor = 0.6;
    _headerValueLabel.backgroundColor = self.headerView.backgroundColor;
    self.headerView.backgroundColor = [UIColor clearColor];

    return _headerValueLabel;
}

- (NSMutableArray*) graphZones{
    if (_graphZones) {
        return _graphZones;
    }
    
    _graphZones = [[NSMutableArray alloc] initWithCapacity:1];
    
    [_graphZones addObject:self.graphLayer];
        
    return _graphZones;
}

- (NSMutableArray*) gridZones{
    if (_gridZones) {
        return _gridZones;
    }
    
    _gridZones = [[NSMutableArray alloc] initWithCapacity:1];
    
    [_gridZones addObject:self.gridLayer];
    
    return _gridZones;
}

- (CGFloat) edgePadding{
    if (_edgePadding<0) {
        return kChartLineViewEdgePadding;
    }
    return _edgePadding;
}

- (CGFloat) zonePadding{
    if (_zonePadding<0) {
        return kChartLineViewEdgePadding;
    }
    
    return _zonePadding;
}

- (CGFloat) mainGrapthLayerHeight{
    if (self.formater && [self.formater respondsToSelector:@selector(numberOfZonesInSChartView:)]) {
        CGFloat height=0;
        
        NSAssert([self.formater respondsToSelector:@selector(meSChartView:zoneAtIndex:)], @"MESChartView: formater must implement - (MESChartZone)meSchartView:zoneAtIndex:, because number zones defined");
        
        for (int i=0; i< [self.formater numberOfZonesInSChartView:self]; i++) {
            MESChartZone *zone  = [self.formater meSChartView:self zoneAtIndex:i];
            height+=zone.height+self.zonePadding;
        }
        return self.rootLayer.bounds.size.height-height;
    }
    return self.rootLayer.bounds.size.height;
}

- (CGRect) rootFrame{
    CGRect rect = CGRectInset(self.layer.bounds, self.edgePadding, self.edgePadding);
    if (!self.titleView.hidden || !self.headerView.hidden) {
        CGFloat h=self.headerView.bounds.size.height-self.edgePadding;
        rect.origin.y+=h;
        rect.size.height-=h;
    }
    return rect;
}

- (CGRect) gridFrame{
    return CGRectMake(0, 0, self.rootLayer.bounds.size.width, self.mainGrapthLayerHeight);
}

- (CGRect) mainGraphFrame{
    return CGRectMake(0, 0, self.rootLayer.bounds.size.width-self.gridLayer.yAxis.width, self.mainGrapthLayerHeight-self.gridLayer.xAxis.width);
}

- (CGFloat) zoneHeihtAtIndex:(NSInteger)index{
    if (self.formater && [self.formater respondsToSelector:@selector(meSChartView:zoneAtIndex:)]) {
        MESChartZone *zone  = [self.formater meSChartView:self zoneAtIndex:index-1];
        return zone.height;
    }
    return kChartZoneHeight;
}

- (CGRect) graphFrameAtZoneIndex:(NSInteger)index{
    CGRect rect = [self gridFrameAtZoneIndex:index];
    rect.size.width -=  self.gridLayer.yAxis.width;
    rect.size.height -=  [self gridLayerAtZoneIndex:index].xAxis.width;
    return rect;
}

- (CGRect) gridFrameAtZoneIndex:(NSInteger)index{
    CGFloat height = self.mainGrapthLayerHeight;
    if (self.formater && [self.formater respondsToSelector:@selector(numberOfZonesInSChartView:)]) {
        
        NSAssert([self.formater respondsToSelector:@selector(meSChartView:zoneAtIndex:)], @"MESChartView: formater must implement - (MESChartZone)meSchartView:zoneAtIndex:, because number zones defined");
        
        for (int i=1; i<[self.formater numberOfZonesInSChartView:self] && i<index; i++) {
            MESChartZone *zone  = [self.formater meSChartView:self zoneAtIndex:i];
            height+=zone.height+self.zonePadding;
        }
    }
    return CGRectMake(0, height+self.zonePadding, self.rootLayer.bounds.size.width, [self zoneHeihtAtIndex:index]);
}


- (CALayer*) rootLayer{
    if (_rootLayer) {
        return _rootLayer;
    }
    
    _rootLayer	= [CALayer layer];
    _rootLayer.frame = [self rootFrame];
    [self.layer addSublayer:_rootLayer];
    
    return _rootLayer;
}

- (MESChartGrid*) gridLayer{
    if (_gridLayer) {
        return _gridLayer;
    }
    
    _gridLayer = [MESChartGrid layer];
    _gridLayer.frame = [self gridFrame];
    [self.rootLayer addSublayer:_gridLayer];
    
    return _gridLayer;
}

- (MESChartCanvas*) graphLayer{
    if (_graphLayer) {
        return _graphLayer;
    }
    
    //
    // main series canvas
    //
    _graphLayer = [MESChartCanvas layer];
    _graphLayer.frame = [self mainGraphFrame];
    
    [_rootLayer insertSublayer:_graphLayer above:self.gridLayer];
    
    return _graphLayer;
}

- (CALayer*) backgroundImageLayer{
    if (_backgroundImageLayer) {
        return _backgroundImageLayer;
    }
    
    _backgroundImageLayer = [CALayer layer];
    _backgroundImageLayer.frame = self.graphLayer.frame;
    
    [_rootLayer insertSublayer:_backgroundImageLayer below:self.graphLayer];
    
    return _backgroundImageLayer;
}

- (MESChartGrid*) gridLayerAtZoneIndex:(NSInteger)index{
    MESChartGrid *gl;
    
    @try {
        gl = [self.gridZones objectAtIndex:index];
    }
    @catch (NSException *exception) {
        if (self.formater && [self.formater respondsToSelector:@selector(meSChartView:zoneAtIndex:)]) {
            MESChartZone *zone  = [self.formater meSChartView:self zoneAtIndex:index-1];
            gl = zone.grid;
        }
        if (!gl) {
            gl = [MESChartGrid layer];
            gl.lineWidth = self.gridLayer.lineWidth;
            gl.strokeColor = self.gridLayer.strokeColor;
            gl.enableGradient = NO;
            gl.yAxis.font = self.gridLayer.yAxis.font;
            gl.yAxis.width = self.gridLayer.yAxis.width;
            gl.yAxis.textColor = self.gridLayer.yAxis.textColor;
        }
        gl.frame = [self gridFrameAtZoneIndex:index];
        [self.gridZones insertObject:gl atIndex:index];
        [self.rootLayer insertSublayer:gl above:self.graphLayer];
    }
    
    return gl;
}

- (MESChartCanvas*) graphLayerAtZoneIndex:(NSInteger)index{
    MESChartCanvas *gl;
    
    @try {
        gl = [self.graphZones objectAtIndex:index];
    }
    @catch (NSException *exception) {
        if (self.formater && [self.formater respondsToSelector:@selector(meSChartView:zoneAtIndex:)]) {
            MESChartZone *zone  = [self.formater meSChartView:self zoneAtIndex:index-1];
            gl = zone.graph;
        }
        
        if (!gl) {
            gl = [MESChartCanvas layer];
            gl.strokeColor = self.graphLayer.strokeColor;
            gl.fillColor = self.graphLayer.fillColor;
            gl.enableGradient = NO;
            gl.lineWidth = self.graphLayer.lineWidth;
        }
        gl.frame = [self graphFrameAtZoneIndex:index];
        [self.graphZones insertObject:gl atIndex:index];
        [self.rootLayer insertSublayer:gl above:self.graphLayer];
    }
    
    return gl;
}

- (CAShapeLayer*) crossHairLayer{
    if (_crossHairLayer) {
        return _crossHairLayer;
    }
    
    _crossHairLayer = [CAShapeLayer layer];
    _crossHairLayer.strokeColor = self.graphLayer.strokeColor;
    _crossHairLayer.lineWidth = self.gridLayer.lineWidth;
    _crossHairLayer.opacity = 0.6;
    _crossHairLayer.drawsAsynchronously = YES;
    _crossHairLayer.shouldRasterize = NO;
    _crossHairLayer.backgroundColor = [UIColor clearColor].CGColor;
    
    CGRect rect =  self.rootLayer.frame;
    _crossHairLayer.frame = rect;
    
    [self.layer insertSublayer:_crossHairLayer below:self.graphLayer];
    
    return _crossHairLayer;
}

- (CAShapeLayer*) crossHairDecorationLayer{
    if (_crossHairDecorationLayer) {
        return _crossHairDecorationLayer;
    }
    
    _crossHairDecorationLayer = [CAShapeLayer layer];
    _crossHairDecorationLayer.strokeColor = self.crossHairLayer.strokeColor;
    _crossHairDecorationLayer.fillColor = [UIColor whiteColor].CGColor;
    _crossHairDecorationLayer.fillRule = kCAFillRuleEvenOdd;
    _crossHairDecorationLayer.lineWidth = self.crossHairLayer.lineWidth;
    _crossHairDecorationLayer.opacity = 1;
    _crossHairDecorationLayer.opaque = YES;
    _crossHairDecorationLayer.allowsEdgeAntialiasing = YES;
    _crossHairDecorationLayer.drawsAsynchronously = YES;
    _crossHairDecorationLayer.backgroundColor = [UIColor clearColor].CGColor;
    _crossHairDecorationLayer.shadowColor = [UIColor grayColor].CGColor;
    _crossHairDecorationLayer.shadowOffset = CGSizeMake(0., 0.);
    _crossHairDecorationLayer.shadowRadius = 2.;
    _crossHairDecorationLayer.shadowOpacity = 1.;
    
    _crossHairDecorationLayer.frame = CGRectMake(0, 0, self.crossHairLayer.bounds.size.width, self.graphLayer.bounds.size.height);
    
    [self.crossHairLayer insertSublayer:_crossHairDecorationLayer above:self.crossHairLayer];
    
    return _crossHairDecorationLayer;
}

- (MESChartCandlesCanvas*) candlesLayer{
    if (_candlesLayer) {
        _candlesLayer.grouthCanvas.frame = _candlesLayer.fallingCanvas.frame = [self mainGraphFrame];
        return _candlesLayer;
    }
    
    _candlesLayer = [MESChartCandlesCanvas layer];
    _candlesLayer.grouthCanvas.frame = _candlesLayer.fallingCanvas.frame = [self mainGraphFrame];
    [self.rootLayer insertSublayer:_candlesLayer.grouthCanvas below:self.graphLayer];
    [self.rootLayer insertSublayer:_candlesLayer.fallingCanvas below:self.graphLayer];
    
    _candlesLayer.fallingCanvas.lineWidth = _candlesLayer.grouthCanvas.lineWidth = self.graphLayer.lineWidth;
    
    return _candlesLayer;
}

- (MESChartSelectionLayer*) selectionLayer{
    if (_selectionLayer) {
        return _selectionLayer;
    }
    
    _selectionLayer = [MESChartSelectionLayer layer];
    _selectionLayer.fillColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.3].CGColor;
    _selectionLayer.opacity = 0.4;
    _selectionLayer.backgroundColor = [UIColor clearColor].CGColor;
    
    [self.rootLayer insertSublayer:_selectionLayer above:self.graphLayer];
    
    return _selectionLayer;
}

- (void) layoutSublayersOfLayer:(CALayer *)layer{
    [super layoutSublayersOfLayer:layer];
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];

    self.titleView.frame = [self titleFrame];
    
    self.rootLayer.frame = [self rootFrame];
    self.gridLayer.frame = [self gridFrame];
    self.graphLayer.frame = [self mainGraphFrame];
    self.backgroundImageLayer.frame = [self mainGraphFrame];
    
    self.candlesLayer.fallingCanvas.frame = self.graphLayer.frame;
    self.candlesLayer.grouthCanvas.frame = self.graphLayer.frame;
    
    self.selectionLayer.frame = self.graphLayer.frame;
    self.crossHairLayer.frame = self.rootLayer.frame;
    
    [CATransaction commit];
}


- (void) setBackgroundImage:(UIImage *)backgroundImage{
    _backgroundImage = backgroundImage;
    self.backgroundImageLayer.contents = (__bridge id)(_backgroundImage.CGImage);
}

-(void) layoutSubviews{
    [super layoutSubviews];
}

#pragma mark - Drawing

- (void) redraw{
    [self setNeedsDisplay];
}

- (CGFloat) maxXWidthAtSeries:(NSInteger)seriesIndex{
    return [self.dataSource meSChartView:self numberOfChartDataForSeriesAtIndex:seriesIndex]*_oneNodeWidth;
}


//
// Draw grid
//
- (__MESChartGridPaths*) drawGrid:(MESChartGrid*)gridLayer onGraph:(MESChartCanvas*)graphLayer atSeriesIndex:(NSInteger)seriesindex{
    
    MESChartDataRange *range = [self.dataSource dataRangeInSChartView:self atSeriesIndex:seriesindex];
    MESChartSeries    *series = [self.dataSource meSChartView:self seriesAtIndex:seriesindex];
    
    double min = [range.minValue doubleValue];
    double max = [range.maxValue doubleValue];
    
    MESChartAxis *yAxis = gridLayer.yAxis;
    MESChartAxis *xAxis = gridLayer.xAxis;
    
    if (yAxis.enableLabels){
        //
        // compute estimated width of Y-Axis
        //
        UILabel *checkLabel = [[UILabel alloc] init];
        UILabel *label = [yAxis labelAtTick:0 inView:self];
        checkLabel.font = yAxis.font;
        checkLabel.bounds = CGRectMake(0, 0, yAxis.width, yAxis.font.pointSize);
        checkLabel.minimumScaleFactor = label.minimumScaleFactor;
        checkLabel.adjustsFontSizeToFitWidth = label.adjustsFontSizeToFitWidth;
        
        if (self.formater && [self.formater respondsToSelector:@selector(meSChartView:yAxisLabel:withValue:atTickIndex:forSeriesAtIndex:)]) {
            [self.formater meSChartView:self yAxisLabel:checkLabel withValue:[NSNumber numberWithDouble:max] atTickIndex:0 forSeriesAtIndex:seriesindex];
        }
        else{
            checkLabel.text = [NSString stringWithFormat:@"%f", max];
        }
        
        if (self.gridLayer==gridLayer){
            self.gridLayer.yAxis.width = [checkLabel sizeThatFits:CGSizeMake(MAXFLOAT, checkLabel.bounds.size.height)].width;
            self.gridLayer.frame = [self gridFrame];
            self.graphLayer.frame = [self mainGraphFrame];
        }
        else{
            yAxis.width = self.gridLayer.yAxis.width;
        }
        label.bounds = CGRectMake(0, 0, yAxis.width, label.bounds.size.height);
    }
    
    __MESChartGridPaths *paths = [[__MESChartGridPaths alloc] init];
    
    CGFloat   x = 0.0f;
    NSInteger lines = yAxis.ticks;
    CGFloat   step = graphLayer.bounds.size.height/(lines-1);
    
    if (step*.7<=xAxis.font.pointSize-xAxis.labelPadding*2-gridLayer.lineWidth*2) {
        lines = 2;
        yAxis.ticks = lines;
        step = graphLayer.bounds.size.height/lines;
    }
    
    CGFloat y = 0;
    
    /**
     *  Horizontal
     */
    CGFloat xpadding=gridLayer.frame.origin.x + gridLayer.bounds.size.width + self.edgePadding + \
    gridLayer.lineWidth*2 - yAxis.width + yAxis.labelPadding;
    
    CGFloat mainHeight = graphLayer.bounds.size.height;
    CGFloat gridWidth  = gridLayer.bounds.size.width;
    CGFloat gridLineWidthPadd = gridLayer.lineWidth*2;
    
    for (int i=0; i<lines; i++, y+=step) {
        CGFloat width = gridWidth;
        // find data value
        double d = [self deNormalizedHeightForY:y withMin:min withMax:max withHeight:mainHeight];
        
        if (lines<=2 && i==1)  width=gridWidth-yAxis.width;
        
        BOOL enableDrawTopLine = YES;
        if (self.formater && [self.formater respondsToSelector:@selector(numberOfZonesInSChartView:)]) {
            NSInteger zoneCount = [self.formater numberOfZonesInSChartView:self];
            if (series.zoneIndex <= zoneCount && i==0 && series.zoneIndex>0) {
                //
                // do not draw top grid line
                //
                if (self.zonePadding<=yAxis.width) {
                    enableDrawTopLine = NO;
                }
            }
        }
        
        if (enableDrawTopLine) {
            BOOL edges = YES;
            if ((i==0 || i==lines-1)&& !yAxis.enableEdges) {
                edges = NO;
            }
            if (edges) {
                CGPathMoveToPoint(paths.yPath, nil, x, y);
                CGPathAddLineToPoint(paths.yPath, nil, width, y);
                CGPathCloseSubpath(paths.yPath);
            }
        }
        
        if (i==0)  y=0;
        
        if (yAxis.enableLabels) {
            UILabel *label = [yAxis labelAtTick:i inView:self];
            CGFloat labelHeight = label.bounds.size.height;
            CGFloat ly;
            CGFloat lx  = xpadding;
            CGFloat estimatedWidh = yAxis.width;
            
            CGFloat lyy = y+labelHeight+gridLayer.frame.origin.y+gridLayer.lineWidth*2;
            BOOL enableTopYLabel = YES;
            if (i==0 && lines>2) {
                ly = lyy;
            }
            else{
                ly = y+gridLayer.frame.origin.y-gridLayer.lineWidth*2.;
                estimatedWidh = [label sizeThatFits:CGSizeMake(MAXFLOAT, label.bounds.size.height)].width;
                if (
                    ((lines<=2 && estimatedWidh>yAxis.width)
                     ||
                     (i==lines-1 && label.bounds.size.height*2<step)
                     )
                    &&
                    enableDrawTopLine
                    ) {
                    if (gridLayer!=self.gridLayer) {
                        enableTopYLabel = NO;
                    }
                    lx = xpadding - estimatedWidh + yAxis.width;
                }
                else{
                    ly = lyy;
                    estimatedWidh = yAxis.width;
                }
            }
            
            if (!self.headerView.hidden || !self.titleView.hidden) {
                ly += self.rootLayer.frame.origin.y-label.bounds.size.height;
            }
            
            if (!enableDrawTopLine && i==0) {
                label.hidden = !enableTopYLabel;
            }
            else{
                label.hidden = !enableTopYLabel;
            }
            
            if (lines<=2)
                if (!(i==0)){
                    continue;
                }
            
            label.frame = CGRectMake(lx, ly, estimatedWidh, labelHeight);
            if (self.formater && [self.formater respondsToSelector:@selector(meSChartView:yAxisLabel:withValue:atTickIndex:forSeriesAtIndex:)]) {
                [self.formater meSChartView:self yAxisLabel:label withValue:[NSNumber numberWithDouble:d] atTickIndex:i forSeriesAtIndex:seriesindex];
            }
            else{
                label.text = [NSString stringWithFormat:@"%f", d];
            }
        }
    }
    
    CGPathMoveToPoint(paths.yPath, nil, x, y);
    CGPathAddLineToPoint(paths.yPath, nil, gridWidth, y);
    CGPathCloseSubpath(paths.yPath);
    
    BOOL adjustToOverMidnight = [self adjustedToOverMidnightInGrid:gridLayer atSeriesIndex:seriesindex];
    
    /**
     *  Vertical
     */
    if (!adjustToOverMidnight) {
        
        CGFloat mainWidth = graphLayer.bounds.size.width;
        
        lines = xAxis.ticks;
        step = mainWidth/(lines-1);
        x = gridLineWidthPadd;
        
        CGFloat gridHeight  = gridLayer.bounds.size.height;
        NSInteger xmax = [self maxXWidthAtSeries:0];
        CGFloat labelY = graphLayer.bounds.size.height+graphLayer.frame.origin.y+self.rootLayer.frame.origin.y + gridLayer.xAxis.labelPadding + gridLayer.lineWidth*2;
        
        for (NSInteger i=0; i<lines; i++, x+=step) {
            
            if (i==0) {
                x=0.0;
            }
            
            // find data index
            NSInteger di = [self deNormalizedIndexForX:x withMax:xmax withWidth:mainWidth];
            MESChartData *data = [self.dataSource meSChartView:self chartDataAtIndex:di forSeriesAtIndex:0];
            
            BOOL edges = YES;
            if ((i==0 || i==lines-1)&& !xAxis.enableEdges) {
                edges = NO;
            }
            
            if (edges) {
                CGPathMoveToPoint(paths.xPath, nil, x, 0);
                CGPathAddLineToPoint(paths.xPath, nil, x, gridHeight);
                CGPathCloseSubpath(paths.xPath);
            }
                        
            if (i<lines-1 && xAxis.enableLabels) {
                [self drawXLabelsInIndex:i atSeriesIndex:seriesindex xPoint:x yPoint:labelY step:step graphLayer:graphLayer gridLayer:gridLayer data:data];
            }
        }
    }
    
    return paths;
}


- (BOOL) adjustedToOverMidnightInGrid:(MESChartGrid*)gridLayer atSeriesIndex:(NSInteger)seriesIndex{
    return [self hasDifferentDaysAtSeriesIndex:seriesIndex] && gridLayer.adjustXGridToOverMidnight;
}

- (void) drawXGridWithPath:(CGMutablePathRef)path inIndex:(NSInteger)index atSeriesIndex:(NSInteger)seriesIndex xPoint:(CGFloat)x yPoint:(CGFloat) labelY step:(CGFloat)step graphLayer:(CAShapeLayer*)graphLayer gridLayer:(MESChartGrid*)gridLayer data:(MESChartData*)aData{    
    
    CGPathMoveToPoint(path, nil, x, 0);
    CGPathAddLineToPoint(path, nil, x, gridLayer.bounds.size.height);
    CGPathCloseSubpath(path);
    
    if (gridLayer.xAxis.enableLabels) {
        [self drawXLabelsInIndex:index atSeriesIndex:seriesIndex xPoint:x yPoint:labelY step:step graphLayer:graphLayer gridLayer:gridLayer data:aData];
    }
}

- (void) drawXLabelsInIndex:(NSInteger)index atSeriesIndex:(NSInteger)seriesIndex xPoint:(CGFloat)x yPoint:(CGFloat)labelY step:(CGFloat)step graphLayer:(CAShapeLayer*)graphLayer gridLayer:(MESChartGrid*)gridLayer data:(MESChartData*)aData{
    
    NSInteger xmax = [self maxXWidthAtSeries:0];
    CGFloat mainWidth = graphLayer.bounds.size.width;
    
    
    // find data index
    NSInteger di = [self deNormalizedIndexForX:x withMax:xmax withWidth:mainWidth];
    MESChartData *data = [self.dataSource meSChartView:self chartDataAtIndex:di forSeriesAtIndex:0];
    
    MESChartAxis *xAxis=gridLayer.xAxis;
    
    UILabel *label = [xAxis labelAtTick:di inView:self];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.frame = CGRectMake(x+xAxis.labelPadding+self.edgePadding+gridLayer.lineWidth*2, labelY, step-xAxis.labelPadding*2-gridLayer.lineWidth*2, label.bounds.size.height);
    
    if (self.formater && [self.formater respondsToSelector:@selector(meSChartView:xAxisLabel:withValue:atTickIndex:forSeriesAtIndex:)]) {
        [self.formater meSChartView:self xAxisLabel:label withValue:data.xValue atTickIndex:index forSeriesAtIndex:seriesIndex];
    }
    else {
        label.text = [NSString stringWithFormat:@"%@", data.xValue];
    }
    
    if (xAxis.width<=0.0) {
        label.hidden = YES;
    }
    else
        label.hidden = NO;
    
}

- (BOOL) hasDifferentDaysAtSeriesIndex:(NSInteger)seriesIndex{
    MESChartData *data_first = [self.dataSource meSChartView:self chartDataAtIndex:0 forSeriesAtIndex:seriesIndex];
    if ([data_first.xValue isKindOfClass:[NSDate class]]) {
        
        NSInteger numbers =[self.dataSource meSChartView:self numberOfChartDataForSeriesAtIndex:seriesIndex];
        MESChartData *data_last = [self.dataSource meSChartView:self chartDataAtIndex: numbers-1 forSeriesAtIndex:seriesIndex];
        
        if (![data_last.xValue isKindOfClass:[NSDate class]]) {
            return NO;
        }
        
        if ( ! [[data_first.xValue dateOnly] isEqual:[data_last.xValue dateOnly]]) {
            return YES;
        }
        
    }
    return NO;
}
 
//
// Draw data
//
- (__MESChartPaths*) drawPathsInLayerForSeriesAtIndex:(NSInteger)seriesIndex inLayer:(CAShapeLayer*) layer inGridLayer:(MESChartGrid*)gridLayer{
    
    MESChartDataRange *range = [self.dataSource dataRangeInSChartView:self atSeriesIndex:seriesIndex];
    
    CGFloat linePadding = self.graphLayer.lineWidth*2+layer.lineWidth*2;
    CGFloat layerHeight = layer.bounds.size.height - linePadding;
    double min = [range.minValue doubleValue];
    double max = [range.maxValue doubleValue];
    
    MESChartSeries *series = [self.dataSource meSChartView:self seriesAtIndex:seriesIndex];
    
    __MESChartPaths *paths = [[__MESChartPaths alloc] init];
    
    NSInteger numbers = [self.dataSource meSChartView:self numberOfChartDataForSeriesAtIndex:seriesIndex];
    
    CGFloat firstY = 0.0, lastY = 0.0, maxWidth=[self maxXWidthAtSeries:seriesIndex];
    float hindex=0.0;
    
    if (series.type != MESCHART_LINE){
        barWidth = layer.bounds.size.width/numbers-layer.lineWidth*2.-self.candlesLayer.widthBetween;
        if (barWidth<0) {
            barWidth=0.2;
        }
    }
    
    CGMutablePathRef selectionPath = CGPathCreateMutable();
    
    if (seriesIndex==0) {
        self.selectionLayer.frame = self.graphLayer.frame;
    }
    
    NSDate *xValueOverMidNight = nil;
    
    /**
     *  Vertical grid adjusted to over midnight
     */
    CGFloat mainWidth = layer.bounds.size.width;
    
    NSInteger lines = gridLayer.xAxis.ticks;
    CGFloat step = mainWidth/(lines-1);
    
    CGFloat labelY = layer.bounds.size.height+layer.frame.origin.y+self.rootLayer.frame.origin.y + gridLayer.xAxis.labelPadding + gridLayer.lineWidth*2;
    
    NSInteger labelIndex = 0;
    CGFloat prevXGrid = 0.0, lastDrawnX=MAXFLOAT;
    
    BOOL adjustToOverMidnight = [self adjustedToOverMidnightInGrid:gridLayer atSeriesIndex:seriesIndex];
    CGFloat betweenStep = 0.0;

    for (int index=0; index<numbers; index++, hindex+=_oneNodeWidth) {
        
        MESChartData *data = [self.dataSource meSChartView:self chartDataAtIndex:index  forSeriesAtIndex:seriesIndex];
        
        if (xValueOverMidNight==nil) {
            xValueOverMidNight = data.xValue;
        }
        
        CGFloat gridx = [self normalizedWidthForRawWidth:hindex withMax:maxWidth withWidth:layer.bounds.size.width];//+layer.lineWidth;
        CGFloat x = gridx + layer.lineWidth;
        
        if (seriesIndex==0) {
            //selection
            CGFloat y = [self normalizedHeightForRawHeight:[data.yValue doubleValue] withMin:min withMax:max withHeight:self.selectionLayer.bounds.size.height];
            if (index==0) {
                CGPathMoveToPoint(selectionPath, nil, x, y);
            }
            else{
                CGPathAddLineToPoint(selectionPath, nil, x, y);
            }
        }
        
        if (adjustToOverMidnight) {
            //
            // Over midnight markers
            //
            
            if ([data.xValue isKindOfClass:[NSDate class]]) {
                if (![[xValueOverMidNight dateOnly] isEqualToDate:[data.xValue dateOnly]] || index==numbers-1){

                    xValueOverMidNight = data.xValue;

                    CGFloat multipleLines =  floor((gridx-prevXGrid)/step);
                    
                    if (gridx-prevXGrid>step || labelIndex==0){
                        
                        if (multipleLines>=1.0) {
                            //
                            // Draw between
                            //
                            
                            betweenStep=(gridx-prevXGrid)/multipleLines;
                            CGFloat beetwenX=prevXGrid;
                            
                            for (int i=0; i<=multipleLines && beetwenX<layer.bounds.size.width+layer.frame.origin.x; beetwenX+=betweenStep, i++) {
                            
                                if (beetwenX==lastDrawnX) {
                                    continue;
                                }
                                                                
                                [self drawXGridWithPath:paths.overMidnightPath inIndex:labelIndex atSeriesIndex:seriesIndex xPoint:beetwenX yPoint:labelY step:betweenStep graphLayer:layer gridLayer:gridLayer data:data];
                                
                                labelIndex++;
                                prevXGrid = gridx;                                
                                lastDrawnX = beetwenX;
                            }
                        }                        
                        else{

                            [self drawXGridWithPath:paths.overMidnightPath inIndex:labelIndex atSeriesIndex:seriesIndex xPoint:gridx yPoint:labelY step:step graphLayer:layer gridLayer:gridLayer data:data];

                            labelIndex++;
                            prevXGrid = lastDrawnX = gridx;                                
                        }
                    }
                }
            }
        }
        
        if (series.type == MESCHART_LINE) {
            CGFloat y = [self normalizedHeightForRawHeight:[data.yValue doubleValue] withMin:min withMax:max withHeight:layer.bounds.size.height];
            
            if (index==0) {
                firstY = y;
                CGPathMoveToPoint(paths.flatPath, nil, x, y);
            }
            else{
                CGPathAddLineToPoint(paths.flatPath, nil, x, y);
            }
            lastY = y;
        }
        else if (series.type == MESCHART_CANDLE || series.type == MESCHART_OHLC){
            
            MESChartOHLC *ohlcv = data.yValue;
            
            CGFloat low   = [self normalizedHeightForRawHeight:[ohlcv.low floatValue] withMin:min withMax:max withHeight:layerHeight]+linePadding;
            CGFloat high  = [self normalizedHeightForRawHeight:[ohlcv.high floatValue] withMin:min withMax:max withHeight:layerHeight]+linePadding;
            CGFloat open  = [self normalizedHeightForRawHeight:[ohlcv.open floatValue] withMin:min withMax:max withHeight:layerHeight]+linePadding;
            CGFloat close = [self normalizedHeightForRawHeight:[ohlcv.close floatValue] withMin:min withMax:max withHeight:layerHeight]+linePadding;
            
            CGFloat ry, bottomStickY, topStickY;
            CGMutablePathRef path;
            
            if (close>open) {
                ry = open;
                bottomStickY = close;
                topStickY = open;
                path = paths.grouthPath;
            }
            else{
                ry = close;
                bottomStickY = open;
                topStickY = close;
                path = paths.fallingPath;
            }
            
            if (series.type==MESCHART_CANDLE) {
                CGRect rect = CGRectMake(x, ry, barWidth, ABS(close-open));
                CGPathAddRect(path, nil, rect);
            }
            
            x = x+barWidth/2.;
            
            CGPathMoveToPoint(path, nil, x, low);
            if (series.type == MESCHART_CANDLE) {
                CGPathAddLineToPoint(path, nil, x, bottomStickY);
                CGPathCloseSubpath(path);
                CGPathMoveToPoint(path, nil, x, topStickY);
            }
            CGPathAddLineToPoint(path, nil, x, high);
            CGPathCloseSubpath(path);
            
            if (series.type == MESCHART_OHLC) {
                CGPathMoveToPoint(path, nil, x-barWidth/2., open);
                CGPathAddLineToPoint(path, nil, x, open);
                CGPathCloseSubpath(path);
                
                CGPathMoveToPoint(path, nil, x, close);
                CGPathAddLineToPoint(path, nil, x+barWidth/2., close);
                CGPathCloseSubpath(path);
            }
            
            CGPathCloseSubpath(path);
        }
        else if (series.type == MESCHART_BAR){
            CGFloat y = [self normalizedHeightForRawHeight:[data.yValue doubleValue] withMin:min withMax:max withHeight:layerHeight];
            CGRect rect = CGRectMake(x, layer.bounds.size.height, barWidth, y-layer.bounds.size.height);
            CGPathMoveToPoint(paths.fallingPath, nil, x, layer.bounds.size.height);
            CGPathAddRect(paths.flatPath, nil, rect);
            CGPathCloseSubpath(paths.flatPath);
        }
    }
    
    if (seriesIndex==0) {
        CGFloat width = self.selectionLayer.bounds.size.width;
        CGFloat height = self.selectionLayer.bounds.size.height;
        
        CGPathAddLineToPoint(selectionPath, nil, width, lastY);
        CGPathAddLineToPoint(selectionPath, nil, width, height);
        
        CGPathAddLineToPoint(selectionPath, nil, 0, height);
        CGPathAddLineToPoint(selectionPath, nil, 0, lastY);
        CGPathCloseSubpath(selectionPath);
        
        self.selectionLayer.path = selectionPath;
        CGPathCloseSubpath(selectionPath);
    }
    
    
    if (series.type == MESCHART_LINE) {
        
        //
        // close line path
        //
        
        CGFloat width = layer.bounds.size.width;
        CGFloat height = layer.bounds.size.height;
        
        CGPathAddLineToPoint(paths.flatPath, nil, width+layer.lineWidth*2, lastY);
        CGPathAddLineToPoint(paths.flatPath, nil, width+layer.lineWidth*2, height+1000);
        
        CGPathAddLineToPoint(paths.flatPath, nil, -layer.lineWidth*2, height+1000);
        CGPathAddLineToPoint(paths.flatPath, nil, -layer.lineWidth*2, firstY);
        
        CGPathCloseSubpath(paths.flatPath);
    }
    
    return paths;
}


- (UIImage *)screenShot{
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions(self.rootLayer.bounds.size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext(self.rootLayer.bounds.size);
    
    [self.rootLayer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void) drawRectIn:(CGRect)rect{
    
    
    NSAssert([self.dataSource respondsToSelector:@selector(meSChartView:seriesAtIndex:)], @"MESChartView: delegate must implement - (MESChartSeries)meSChartView:seriesAtIndex:");
    NSAssert([self.dataSource respondsToSelector:@selector(numberOfSeriesInSChartView:)], @"MESChartView: delegate must implement - (NSIntegre)numberOfSeriesInSChartView");
    NSAssert([self.dataSource respondsToSelector:@selector(meSChartView:numberOfChartDataForSeriesAtIndex:)], @"MESChartView: delegate must implement - (NSInteger)meSChartView:numberOfChartDataForSeriesAtIndex:");
    NSAssert([self.dataSource respondsToSelector:@selector(meSChartView:chartDataAtIndex:forSeriesAtIndex:)], @"MESChartView: delegate must implement - (MESChartData*)meSChartView:chartDataAtIndex:forSeriesAtIndex:");
    NSAssert([self.dataSource respondsToSelector:@selector(dataRangeInSChartView:atSeriesIndex:)], @"MESChartView: delegate must implement - (MESChartRange*)dataYRangeInSChartView");
    
    NSInteger series = [self.dataSource numberOfSeriesInSChartView:self];
    
    if (series==0) {
        return;
    }    

    for (int seriesIndex=0; seriesIndex<series; seriesIndex++) {
                        
        MESChartSeries *series = [self.dataSource meSChartView:self seriesAtIndex:seriesIndex];
        
        if (series.zoneIndex>0 && !self.formater) {
            [NSException raise:@"MESChartView zone index beyond bounds:" format:@"zone index >0 but self.formater: is not defined"];
        }
        else if (series.zoneIndex>0 && ![self.formater respondsToSelector:@selector(numberOfZonesInSChartView:)]){
            [NSException raise:@"MESChartView zone index beyond bounds:" format:@"zone index >0 but numberOfZonesInSChartView: is not defined"];
        }
        else if (series.zoneIndex-1>=[self.formater numberOfZonesInSChartView:self]) {
            [NSException raise:@"MESChartView zone index beyond bounds:" format:@"index=%li zone count: %lu", (long)series.zoneIndex, (unsigned long)[self.formater numberOfZonesInSChartView:self]];
        }

        MESChartGrid *zoneGrid;
        id zone;
        
        if (seriesIndex==0){
            if(series.type == MESCHART_CANDLE || series.type == MESCHART_OHLC) {
                zone = self.candlesLayer;
            }
            else
                zone = self.graphLayer;
        }
        else{
            zone = [self graphLayerAtZoneIndex:series.zoneIndex];
            CGRect zoneRect = [self graphFrameAtZoneIndex:series.zoneIndex];
            [zone setFrame: zoneRect];
        }
        
        //
        // zone grid
        //
        if (seriesIndex==0) {
            zoneGrid = self.gridLayer;
        }
        else{
            zoneGrid = [self gridLayerAtZoneIndex:series.zoneIndex];
            CGRect zoneGridRect = [self gridFrameAtZoneIndex:series.zoneIndex];
            zoneGrid.frame = zoneGridRect;
        }
        
        [zoneGrid.xAxis flushLabelsInView:self];

        //
        // redraw series
        //
        __MESChartPaths *paths;
        __MESChartGridPaths *gridZonePaths;
        if (series.type == MESCHART_CANDLE || series.type == MESCHART_OHLC) {
        
            //
            // firstly, draw grid
            //
            gridZonePaths = [self drawGrid:zoneGrid onGraph:[zone grouthCanvas] atSeriesIndex:seriesIndex];

            paths = [self drawPathsInLayerForSeriesAtIndex:seriesIndex inLayer:[zone grouthCanvas] inGridLayer:zoneGrid];
            [[zone grouthCanvas] setPath:paths.fallingPath animated:self.enableRedrawAnimation];
            [[zone fallingCanvas] setPath:paths.grouthPath animated:self.enableRedrawAnimation];
        }
        else{
            gridZonePaths = [self drawGrid:zoneGrid onGraph:zone atSeriesIndex:seriesIndex];
            
            paths = [self drawPathsInLayerForSeriesAtIndex:seriesIndex inLayer:zone inGridLayer:zoneGrid];
            [zone setPath:paths.flatPath animated:self.enableRedrawAnimation];
        }

        [zoneGrid.yLayer setPath:gridZonePaths.yPath animated:self.enableRedrawAnimation];
        
        if ([self adjustedToOverMidnightInGrid:zoneGrid atSeriesIndex:seriesIndex]) {
            [zoneGrid.xLayer setPath:paths.overMidnightPath animated:self.enableRedrawAnimation];
        }
        else{
            [zoneGrid.xLayer setPath:gridZonePaths.xPath animated:self.enableRedrawAnimation];
        }
        
    }
}

- (void) drawRect:(CGRect)rect{
    [self drawRectIn:rect];
}


#pragma mark - Draw selection

- (CGPoint) drawCrossHairAtFinger:(CGPoint)touchPointIn forSeriesIndex:(NSInteger)seriesIndex inCrossPath:(CGMutablePathRef)crossHairPath inArcPath:(CGMutablePathRef)arcPath{
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];

    MESChartSeries *series = [self.dataSource meSChartView:self seriesAtIndex:seriesIndex];
    CGPoint touchPoint=touchPointIn;
    
    CGFloat mainWidth =  self.selectionLayer.bounds.size.width;
    NSInteger xmax = [self maxXWidthAtSeries:seriesIndex];
    
    NSInteger di = [self deNormalizedIndexForX:touchPointIn.x withMax:xmax withWidth:mainWidth];
    if (di<0)   di=0;
    
    NSInteger count = [self.dataSource meSChartView:self numberOfChartDataForSeriesAtIndex:seriesIndex];
    if (di>=count) di=count-1;
    
    CGFloat x=[self normalizedWidthForRawWidth:di withMax:xmax withWidth:self.graphLayer.bounds.size.width];
    
    if (series.type == MESCHART_CANDLE || series.type == MESCHART_OHLC) {
        x+=barWidth/2.+self.candlesLayer.grouthCanvas.lineWidth-self.crossHairLayer.lineWidth/2.;
    }
    else{
        x+=self.graphLayer.lineWidth;
    }
    
    //
    // "crosshair" (just only one line)
    //
    
    CGPathMoveToPoint(crossHairPath, nil, x, 0);
    CGPathAddLineToPoint(crossHairPath, nil, x, self.crossHairLayer.bounds.size.height);
    CGPathCloseSubpath(crossHairPath);
    
    //
    // point
    //
    CGFloat radius;
    if (series.type == MESCHART_CANDLE || series.type == MESCHART_OHLC) {
        radius = barWidth+self.crossHairLayer.lineWidth*2+self.candlesLayer.widthBetween;
    }
    else{
        radius = self.graphLayer.lineWidth*2.+self.crossHairLayer.lineWidth*.2;
    }
    
    CGFloat y = series.type==MESCHART_CANDLE||series.type==MESCHART_OHLC?touchPoint.y-self.graphLayer.lineWidth*2.+radius/2.:touchPoint.y-radius/2.;
    CGPathAddEllipseInRect(arcPath, nil, CGRectMake(x-radius/2., y, radius, radius));
    CGPathCloseSubpath(arcPath);
    
    touchPoint.x=x;
    
    [CATransaction commit];
    
    return touchPoint;
}

- (void) drawCrossHairsAtFinger1:(CGPoint)touchPointIn andFinger2:(CGPoint)touchPointIn2 forSeriesIndex:(NSInteger)seriesIndex hasTouched2:(BOOL)hasTouched2{
    
    CGPoint touchPoint=touchPointIn, touchPoint2=touchPointIn2;
    
    NSInteger  dataCount = [self.dataSource meSChartView:self numberOfChartDataForSeriesAtIndex:seriesIndex];
    
    if (dataCount<=1) {
        return;
    }
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];

    if (fabs(lastTouched.x-touchPoint.x) > barWidth || lastTouched.x<0 || fabs(lastTouched2.x-touchPoint2.x) > barWidth || lastTouched.x>=lastTouched2.x) {
        MESChartSeries *series = [self.dataSource meSChartView:self seriesAtIndex:seriesIndex];
        
        CGFloat mainWidth = self.graphLayer.bounds.size.width;
        NSInteger xmax = [self maxXWidthAtSeries:seriesIndex];
        CGMutablePathRef crossHairPath = CGPathCreateMutable();
        CGMutablePathRef arcPath = CGPathCreateMutable();
        
        NSInteger index = [self deNormalizedIndexForX:touchPoint.x withMax:xmax withWidth:mainWidth];
        
        CGPoint lastPoint=lastTouched;
        
        if (index>=0 && index<dataCount) {
            lastTouched = [self dataPointAtDataIndex:index forSeriesIndex:seriesIndex];
            
            if (series.type == MESCHART_OHLC || series.type == MESCHART_CANDLE) {
                CGFloat x = [self normalizedWidthForRawWidth:index withMax:[self maxXWidthAtSeries:seriesIndex] withWidth:mainWidth];//+(self.candlesLayer.grouthCanvas.lineWidth+barWidth)/2.;
                lastTouched.x=x;
            }
            
        }
        
        CGPoint tp1,tp2;
        
        if (lastTouched.x>=lastPoint.x-barWidth/2. && lastTouched.x<=lastPoint.x+barWidth/2.) {
            // nothing to redraw
            if (!hasTouched2){
                [CATransaction commit];
                return;
            }
        }
        
        if (hasTouched2) {
            NSInteger index2 = [self deNormalizedIndexForX:touchPoint2.x withMax:xmax withWidth:mainWidth];
            if (index2<0) index2 = 0;
            if (index2>=dataCount) index2=dataCount-1;
            
            lastTouched2 = [self dataPointAtDataIndex:index2 forSeriesIndex:seriesIndex];
            
            tp1=[self drawCrossHairAtFinger:lastTouched forSeriesIndex:seriesIndex inCrossPath:crossHairPath inArcPath:arcPath];
            tp2=[self drawCrossHairAtFinger:lastTouched2 forSeriesIndex:seriesIndex inCrossPath:crossHairPath inArcPath:arcPath];
            
            [self drawSelectionBetweenFinger1:tp1 andFinger2:tp2 forSeriesIndex:seriesIndex withIndexForFinger1:index withIndexForFinger2:index2];
        }
        else{
            
            tp1=[self drawCrossHairAtFinger:lastTouched forSeriesIndex:seriesIndex inCrossPath:crossHairPath inArcPath:arcPath];
            
            CGFloat x = lastTouched.x-self.headerView.bounds.size.width/2.+self.rootLayer.frame.origin.x+self.graphLayer.frame.origin.x;
            [self moveHeaderToX:x withLeftIndex:index withRightIndex:-1 forSeriesIndex:seriesIndex];
        }
        
        [self.crossHairLayer setPath:crossHairPath animated:NO];
        [self.crossHairDecorationLayer setPath:arcPath animated:NO];
        
        CGPathRelease(crossHairPath);
        CGPathRelease(arcPath);
    }
    
    [CATransaction commit];
}

- (void) drawSelectionBetweenFinger1:(CGPoint)touchPointIn andFinger2:(CGPoint)touchPointIn2 forSeriesIndex:(NSInteger)seriesIndex withIndexForFinger1:(NSInteger)index1 withIndexForFinger2:(NSInteger)index2{
    CGFloat x = fmin(touchPointIn.x, touchPointIn2.x);
    CGFloat x2 = fmax(touchPointIn2.x, touchPointIn.x);
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];

    self.selectionLayer.selection = CGRectMake(x, 0, x2-x, self.selectionLayer.bounds.size.height);
    
    CGFloat xmid=CGRectGetMidX(CGRectMake(x, 10, x2-x, self.selectionLayer.bounds.size.height))-self.headerView.bounds.size.width/2.-self.graphLayer.frame.origin.x+self.rootLayer.frame.origin.x;
    [self moveHeaderToX:xmid withLeftIndex:index1 withRightIndex:index2 forSeriesIndex:seriesIndex];
    
    [CATransaction commit];
}


- (void) moveHeaderToX:(CGFloat)x withLeftIndex:(NSInteger)indexLeft withRightIndex:(NSInteger)indexRight forSeriesIndex:(NSInteger)seriesIndex{
    
    x = fmax(x, self.graphLayer.frame.origin.x+self.rootLayer.frame.origin.x);
    if (x+self.headerView.bounds.size.width>=self.graphLayer.frame.origin.x+self.rootLayer.frame.origin.x+self.graphLayer.bounds.size.width) {
        x=self.graphLayer.frame.origin.x+self.rootLayer.frame.origin.x+self.graphLayer.bounds.size.width-self.headerView.bounds.size.width;
    }
    
    CGFloat duration=[UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    CGRect frame = CGRectMake(x, self.headerView.frame.origin.y, self.bounds.size.width/3., self.headerView.bounds.size.height);
    NSInteger count = [self.dataSource meSChartView:self numberOfChartDataForSeriesAtIndex:seriesIndex];
    
    MESChartData *left = indexLeft>=0&&indexLeft<count?[self.dataSource meSChartView:self chartDataAtIndex:indexLeft forSeriesAtIndex:seriesIndex]:nil;
    MESChartData *right = indexRight>=0&&indexRight<count?[self.dataSource meSChartView:self chartDataAtIndex:indexRight forSeriesAtIndex:seriesIndex]:nil;
    
    if (self.formater && [self.formater respondsToSelector:@selector(meSChartView:headerView:atLeftFingerData:atRightFingerData:)]) {
        [self.formater meSChartView:self headerView:self.headerValueLabel atLeftFingerData:left atRightFingerData:right];
    }
    else{
        self.headerValueLabel.text = [NSString stringWithFormat:@"%@:%@",left.yValue,right.yValue];
    }
    
    if (self.headerView.hidden) return;
    
    if (!self.headerInProgress) {
        self.headerInProgress=YES;
        self.headerView.frame = frame;
        [UIView animateWithDuration:duration animations:^{
            self.headerView.alpha = 1.0;
            self.titleView.alpha = 0.0;
        }];
    }
    else{
        self.headerView.frame = frame;
        
//        [UIView transitionWithView:self.headerView
//                          duration:duration/4.0f
//                           //options:UIViewAnimationOptionTransitionCrossDissolve
//                           options:UIViewAnimationOptionCurveLinear
//                        animations:^{
//                               self.headerView.frame = frame;
//                        }
//                        completion:nil];
    }
}


#pragma mark - Touches event handlers

- (void) cancelTouches{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(touchesBeganDeferred:) object:lastTouchesBegan];

    self.crossHairLayer.path = nil;
    self.crossHairDecorationLayer.path = nil;
    lastTouched  = CGPointMake(-1., -1.);
    lastTouched2 = CGPointMake(-1., -1.);
    [self.selectionLayer unselect];
    if (!self.headerView.hidden) {
        [UIView animateWithDuration:0.1 animations:^{
            self.headerView.alpha = 0.0;
            self.titleView.alpha = 1.0;
        }];
    }
    self.headerInProgress = NO;
    isTouched = NO;
}


- (void) pinchHandler:(UIPinchGestureRecognizer*)gesture{
    CGPoint touchPoint = [gesture locationOfTouch:0 inView:self];
    BOOL    hasTwoToches = gesture.numberOfTouches>1;
    CGPoint touchPoint2 = !hasTwoToches?CGPointMake(self.rootLayer.bounds.size.width, 0):[gesture locationOfTouch:1 inView:self];
    
    CGFloat x = touchPoint.x;
    CGFloat x2 = touchPoint2.x;
    
    CGPoint touchPointSaved = touchPoint;
    touchPoint = x<x2?touchPoint:touchPoint2;
    touchPoint2 = x<x2?touchPoint2:touchPointSaved;
    
    touchPoint.x = touchPoint.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;
    touchPoint2.x = touchPoint2.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;
    
    NSInteger seriesIndex = 0;
    CGFloat mainWidth =  self.selectionLayer.bounds.size.width;
    NSInteger xmax = [self maxXWidthAtSeries:seriesIndex];
    NSInteger index = [self deNormalizedIndexForX:touchPoint.x withMax:xmax withWidth:mainWidth];
    NSInteger index2 = [self deNormalizedIndexForX:touchPoint2.x withMax:xmax withWidth:mainWidth];
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(meSChartView:beganTouchWithPoints:withDataIndex:atSeriesindex:)] && !isTouched) {
            [self.delegate meSChartView:self
                   beganTouchWithPoints:@[[NSValue valueWithCGPoint:touchPoint],[NSValue valueWithCGPoint:touchPoint2]]
                          withDataIndex:@[[NSNumber numberWithInteger:index], [NSNumber numberWithInteger:index2]]
                          atSeriesindex:seriesIndex];
            isTouched = YES;
        }
    }
    else if(gesture.state == UIGestureRecognizerStateChanged){
        if (gesture.numberOfTouches==1) {
            [self.selectionLayer unselect];
        }
        [self drawCrossHairsAtFinger1:touchPoint andFinger2:touchPoint2 forSeriesIndex:0 hasTouched2:hasTwoToches];
    }
    else  if (gesture.state == UIGestureRecognizerStateEnded){
        [self cancelTouches];
        if (self.delegate && [self.delegate respondsToSelector:@selector(meSChartView:finishedTouchWithPoints:withDataIndex:atSeriesindex:)]) {
            [self.delegate meSChartView:self
                finishedTouchWithPoints:@[[NSValue valueWithCGPoint:touchPoint],[NSValue valueWithCGPoint:touchPoint2]]
                          withDataIndex:@[[NSNumber numberWithInteger:index],[NSNumber numberWithInteger:index2]]
                          atSeriesindex:seriesIndex
             ];
        }
    }
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    if (gestureRecognizer==panGesture) {
        return isTouched==NO;
    }
    return NO;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touchFirst, *touchSecond = nil;
    
    if (!isTouched) {
        return;
    }

    int i=0;
    for (UITouch *t in touches) {
        if (i==0)
            touchFirst = t;
        else
            touchSecond = t;
        i++; if (i>1) break;
    }

    //BOOL hasSecondTouch=touchSecond!=nil;
    
    CGPoint touchPoint  = [touchFirst locationInView:self];
    //CGPoint touchPoint2 = hasSecondTouch?[touchSecond locationInView:self]:touchPoint;

    touchPoint.x = touchPoint.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;
    //touchPoint2.x = touchPoint2.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;

    if (touchPoint.x<0) {
        touchPoint.x = 0;
    }
    
    NSInteger seriesIndex = 0;
    
    //touchPoint2=hasSecondTouch?touchPoint2:touchPoint;
    
    [self drawCrossHairsAtFinger1:touchPoint andFinger2:touchPoint forSeriesIndex:seriesIndex hasTouched2:NO];
}


- (void) touchesBeganDeferred:(NSSet *)touches{
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];

    self.crossHairDecorationLayer.frame = CGRectMake(0, 0, self.crossHairLayer.bounds.size.width, self.graphLayer.bounds.size.height);
    
    [CATransaction commit];
    
    UITouch *touchFirst, *touchSecond = nil;
    
    int i=0;
    for (UITouch *t in touches) {
        if (i==0)
            touchFirst = t;
        else
            touchSecond = t;
        i++; if (i>1) break;
    }
    
    BOOL hasSecondTouch=touchSecond!=nil;
    
    CGPoint touchPoint  = [touchFirst locationInView:self];
    CGPoint touchPoint2 = [touchSecond locationInView:self];
    
    if (touchPoint.x<0) {
        touchPoint.x = 0;
    }
    
    //
    // TODO: remove the copy paste!
    //
    if (hasSecondTouch) {
        CGFloat x = touchPoint.x;
        CGFloat x2 = touchPoint2.x;
        
        CGPoint touchPointSaved = touchPoint;
        touchPoint = x<x2?touchPoint:touchPoint2;
        touchPoint2 = x<x2?touchPoint2:touchPointSaved;
    }
    
    touchPoint.x = touchPoint.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;
    touchPoint2.x = touchPoint2.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;
    
    NSInteger seriesIndex = 0;
    CGFloat mainWidth =  self.selectionLayer.bounds.size.width;
    NSInteger xmax = [self maxXWidthAtSeries:seriesIndex];
    NSInteger index = [self deNormalizedIndexForX:touchPoint.x withMax:xmax withWidth:mainWidth];
    NSInteger index2 = [self deNormalizedIndexForX:touchPoint2.x withMax:xmax withWidth:mainWidth];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(meSChartView:beganTouchWithPoints:withDataIndex:atSeriesindex:)] && !isTouched) {
        NSArray *ts = hasSecondTouch?@[[NSValue valueWithCGPoint:touchPoint],[NSValue valueWithCGPoint:touchPoint2]]:@[[NSValue valueWithCGPoint:touchPoint]];
        NSArray *is = hasSecondTouch?@[[NSNumber numberWithInteger:index], [NSNumber numberWithInteger:index2]]:@[[NSNumber numberWithInteger:index]];
        
        [self.delegate meSChartView:self
               beganTouchWithPoints:ts
                      withDataIndex:is
                      atSeriesindex:seriesIndex];
    }
    
    isTouched = YES;
    
    [self drawCrossHairsAtFinger1:touchPoint andFinger2:touchPoint2 forSeriesIndex:0 hasTouched2:hasSecondTouch];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    lastTouchesBegan = touches;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(touchesBeganDeferred:) object:lastTouchesBegan];
    [self performSelector:@selector(touchesBeganDeferred:) withObject:touches afterDelay:0.1];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if (!isTouched) {
        [self cancelTouches];
        return;
    }
    
    UITouch *touchFirst, *touchSecond = nil;
    
    int i=0;
    for (UITouch *t in touches) {
        if (i==0)
            touchFirst = t;
        else
            touchSecond = t;
        i++; if (i>1) break;
    }
    
    BOOL hasSecondTouch=touchSecond!=nil;
    
    CGPoint touchPoint  = [touchFirst locationInView:self];
    CGPoint touchPoint2 = [touchSecond locationInView:self];
    
    touchPoint.x = touchPoint.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;
    touchPoint2.x = touchPoint2.x-self.graphLayer.frame.origin.x-self.rootLayer.frame.origin.x;
    
    if (touchPoint.x<0 || (hasSecondTouch && touchPoint2.x<0)) {
        return;
    }
    
    NSInteger seriesIndex = 0;
    CGFloat mainWidth =  self.selectionLayer.bounds.size.width;
    NSInteger xmax = [self maxXWidthAtSeries:seriesIndex];
    NSInteger index = [self deNormalizedIndexForX:touchPoint.x withMax:xmax withWidth:mainWidth];
    NSInteger index2 = [self deNormalizedIndexForX:touchPoint2.x withMax:xmax withWidth:mainWidth];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(meSChartView:beganTouchWithPoints:withDataIndex:atSeriesindex:)]) {
        
        NSArray *ts = hasSecondTouch?@[[NSValue valueWithCGPoint:touchPoint],[NSValue valueWithCGPoint:touchPoint2]]:@[[NSValue valueWithCGPoint:touchPoint]];
        NSArray *is = hasSecondTouch?@[[NSNumber numberWithInteger:index], [NSNumber numberWithInteger:index2]]:@[[NSNumber numberWithInteger:index]];
        
        [self.delegate meSChartView:self
            finishedTouchWithPoints:ts
                      withDataIndex: is
                      atSeriesindex:seriesIndex
         ];
    }
    
    [self cancelTouches];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [self cancelTouches];
}

#pragma mark Utils

- (CGPoint) dataPointAtDataIndex:(NSInteger)index forSeriesIndex:(NSInteger)seriesIndex{
    MESChartSeries *series = [self.dataSource meSChartView:self seriesAtIndex:seriesIndex];
    MESChartDataRange *range = [self.dataSource dataRangeInSChartView:self atSeriesIndex:seriesIndex];
    double min = [range.minValue doubleValue];
    double max = [range.maxValue doubleValue];
    
    MESChartData *data = [self.dataSource meSChartView:self chartDataAtIndex:index forSeriesAtIndex:seriesIndex];
    
    CGFloat y = [self normalizedHeightForRawHeight:[data.yValue doubleValue] withMin:min withMax:max withHeight:self.crossHairDecorationLayer.bounds.size.height];
    CGFloat x = [self normalizedWidthForRawWidth:index withMax:[self.dataSource meSChartView:self numberOfChartDataForSeriesAtIndex:seriesIndex] withWidth:self.graphLayer.bounds.size.width];
    
    if ([data.yValue isKindOfClass:[MESChartOHLC class]]) {
        if (series.type == MESCHART_CANDLE || series.type == MESCHART_OHLC) {
            MESChartOHLC *ohlc = data.yValue;
            CGFloat high  = [self normalizedHeightForRawHeight:[ohlc.open floatValue] withMin:min withMax:max withHeight:self.crossHairDecorationLayer.bounds.size.height];
            CGFloat low = [self normalizedHeightForRawHeight:[ohlc.close floatValue] withMin:min withMax:max withHeight:self.crossHairDecorationLayer.bounds.size.height];
            y +=  (high-low)/2.;
        }
    }
    
    return CGPointMake(x, y);
}

- (double) deNormalizedHeightForY:(double)y withMin:(double)min withMax:(double)max withHeight:(double)height
{
    if (height==0.0 || (max-min)==0.0) {
        return 0.0;
    }
    return (((height-y)/height)*(max-min))+min;
}

- (double)normalizedHeightForRawHeight:(double)rawHeight withMin:(double)minHeight withMax:(double)maxHeight withHeight:(double)height
{
    if ((maxHeight - minHeight)==0.0) {
        return 0.0;
    }
    return height - ((rawHeight - minHeight) / (maxHeight - minHeight)) * height;
}

- (NSInteger) deNormalizedIndexForX:(double)x withMax:(double)maxWidth withWidth:(double)width{
    if (x>width) {
        x=width;
    }
    NSInteger ret = (NSInteger) floor(((x+0.5)/width)*maxWidth);
    if (ret<0) {
        ret = 0;
    }
    if (ret>=maxWidth) {
        ret=maxWidth-1;
    }
    return ret;
}

- (double)normalizedWidthForRawWidth:(double)rawWidth withMax:(double)maxWidth withWidth:(double)width
{
    return ((rawWidth) / (maxWidth)) * width;
}

@end
