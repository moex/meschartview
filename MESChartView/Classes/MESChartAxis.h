//
//  MESChartAxis.h
//  MESChartView
//
//  Created by denis svinarchuk on 26.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MESChartAxis : NSObject

@property (nonatomic,strong) UIFont   *font;
@property (nonatomic,strong) UIColor  *textColor;
@property (nonatomic,assign) NSInteger ticks;
@property (nonatomic,assign) CGFloat   width;
@property (nonatomic,assign) CGFloat   labelPadding;
@property (nonatomic,assign) BOOL      enableEdges;
@property (nonatomic,assign) BOOL      enableLabels;

- (UILabel*) labelAtTick:(NSInteger)index inView:(UIView *)view;
- (void) flushLabelsInView:(UIView*)view;

@end
