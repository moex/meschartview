//
//  MESChartProtocols.h
//  Pods
//
//  Created by denis svinarchuk on 09.03.14.
//
//

#import <Foundation/Foundation.h>
#import "MESChartSeries.h"

@class MESChartView;

/**
 *  
 */
@protocol MESChartFormater <NSObject>
@optional
- (void) meSChartView:(MESChartView*)chart yAxisLabel:(UILabel*)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex;
- (void) meSChartView:(MESChartView*)chart xAxisLabel:(UILabel*)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex;
- (void) meSChartView:(MESChartView*)chart headerView:(UILabel*)label atLeftFingerData:(MESChartData*)leftData atRightFingerData:(MESChartData*)rightData;
- (MESChartZone*) meSChartView:(MESChartView*)chart zoneAtIndex:(NSInteger)zoneIndex;
- (NSInteger) numberOfZonesInSChartView:(MESChartView*)chart;
@end

/**
 *  
 */
@protocol MESChartDataSource <NSObject>
- (MESChartSeries*) meSChartView:(MESChartView*)chart seriesAtIndex:(NSInteger)index;
- (NSInteger) numberOfSeriesInSChartView:(MESChartView*)chart;
- (NSInteger) meSChartView:(MESChartView*)chart numberOfChartDataForSeriesAtIndex:(NSInteger)index;
- (MESChartData*) meSChartView:(MESChartView*)chart chartDataAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex;
- (MESChartDataRange*) dataRangeInSChartView:(MESChartView*)chart atSeriesIndex:(NSInteger)index;
@end


/**
 *  
 */
@protocol MESChartDelegate <NSObject>

@optional

- (void) meSChartView:(MESChartView*)chart beganTouchWithPoints:(NSArray*)points withDataIndex:(NSArray*)indxs atSeriesindex:(NSInteger)seriesIndex;
- (void) meSChartView:(MESChartView*)chart finishedTouchWithPoints:(NSArray*)points withDataIndex:(NSArray*)indxs atSeriesindex:(NSInteger)seriesIndex;

@end
