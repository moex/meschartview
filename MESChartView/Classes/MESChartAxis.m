//
//  MESChartAxis.m
//  MESChartView
//
//  Created by denis svinarchuk on 26.02.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartAxis.h"

CGFloat static const   kChartAxisFontSize = 14.0f;
CGFloat static const   kChartAxisWidth = 10.0f;
CGFloat static const   kChartAxisLabelPadding = 2.0f;
NSInteger static const kChartAxisTicks = 5;

@interface MESChartAxis()
@property (nonatomic,strong) NSMutableDictionary *labels;
@end

@implementation MESChartAxis

@synthesize width=_width;

- (id) init{
    self = [super init];
    
    if (self) {
        _width = -1;
        _labelPadding = -1;
        _ticks = -1;
        _font = nil;
        _enableEdges = NO;
        _enableLabels = YES;
    }
    
    return self;
}

- (NSInteger) ticks{
    if (_ticks<0) {
        return kChartAxisTicks;
    }
    return _ticks;
}


- (NSMutableDictionary*) labels{
    if (_labels) {
        return _labels;
    }
    
    _labels = [[NSMutableDictionary alloc] init];
    
    return _labels;
}

- (CGFloat) width{
    if (_width<0) {
        return kChartAxisWidth;
    }
    
    return _width;
}

- (void) setWidth:(CGFloat)width{
    _width=width;
}

- (CGFloat) labelPadding{
    if (_labelPadding<0) {
        return kChartAxisLabelPadding;
    }
    return _labelPadding;
}


- (UIFont*) font{
    if (!_font) {
        return [UIFont systemFontOfSize:kChartAxisFontSize];
    }    
    return _font;
}

- (UIColor*) textColor{
    if (!_textColor) {
        return [UIColor blackColor];
    }
    return _textColor;
}

- (void) flushLabelsInView:(UIView *)view{
    //NSNumber *key = [NSNumber numberWithInteger:index+(unsigned long long)(__bridge void*)self];

    for (NSNumber *key in self.labels) {
        UILabel *label=[self.labels objectForKey:key];
        [label removeFromSuperview];
    }
    
    [self.labels removeAllObjects];
}

- (UILabel*) labelAtTick:(NSInteger)index  inView:(UIView *)view{
    
//    if (!(index>=0 && index<self.ticks)) {
//        [NSException raise:@"MESChartAxis: index ticks out of bounds" format:@"[0, %li] index=%li", (long)self.ticks, (long)index];        
//    }
        
    NSNumber *key = [NSNumber numberWithInteger:index];
    UILabel *label = [self.labels objectForKey:key];
    //NSInteger tag = index+981;
    //UILabel *label = (UILabel*)[view viewWithTag:tag];

    
    if (!label) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.width, self.font.pointSize)];
        label.font = self.font;
        label.minimumScaleFactor = 0.7;
        label.adjustsFontSizeToFitWidth = YES;
        label.textColor = self.textColor;
        label.textAlignment = NSTextAlignmentRight;
        label.tag = index;
        
        [self.labels setObject:label forKey:key];
        [view addSubview:label];
    }    


    label.tag=index;
    
    return label;
}


@end
