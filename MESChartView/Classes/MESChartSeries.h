//
//  MESChartSeries.h
//  MESChartView
//
//  Created by denis svinarchuk on 2/27/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MESChartAxis.h"
#import "MESChartGrid.h"
#import "MESChartCanvas.h"

typedef enum {
    MESCHART_LINE,
    MESCHART_CANDLE,
    MESCHART_OHLC,
    MESCHART_BAR,
}MESChartSeriesType;

@interface MESChartSeries : NSObject
@property (nonatomic,assign) MESChartSeriesType type;
@property (nonatomic,assign) NSInteger zoneIndex;
@end

/**
 *  Custom zones. Every zone lays under the main grapth.
 *  Index begins from 0.
 */
@interface MESChartZone : NSObject
@property (nonatomic,strong) MESChartCanvas *graph;
@property (nonatomic,strong) MESChartGrid *grid;
@property (nonatomic,assign) CGFloat height;
@end