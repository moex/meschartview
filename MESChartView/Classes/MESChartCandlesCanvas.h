//
//  MESChartCandlesCanvas.h
//  MESChartView
//
//  Created by denis svinarchuk on 01.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MESChartCanvas.h"

@interface MESChartCandlesCanvas : NSObject
+ (id) layer;
@property (nonatomic,readonly) MESChartCanvas *grouthCanvas;
@property (nonatomic,readonly) MESChartCanvas *fallingCanvas;
@property (nonatomic,assign) CGFloat widthBetween;
@end
