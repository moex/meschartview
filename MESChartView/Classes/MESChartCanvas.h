//
//  MESChartShape.h
//  MESChartView
//
//  Created by denis svinarchuk on 2/26/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CAShapeLayer (MESAnimated)
+ (void) setAnimationDuration:(NSTimeInterval)duration;
- (void) setPath:(CGPathRef)path animated:(BOOL)animated;
@end

@interface MESChartCanvas : CAShapeLayer
@property (nonatomic,assign) BOOL enableGradient;
@property (nonatomic,assign) CGColorRef gradientColor;
@end

@interface MESChartSelectionLayer : CAShapeLayer
@property (nonatomic,assign) CGRect selection;
- (void) unselect;
@end