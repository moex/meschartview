//
//  MESChartGrid.h
//  MESChartView
//
//  Created by denis svinarchuk on 2/26/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartCanvas.h"
#import "MESChartAxis.h"


@interface MESChartGrid : MESChartCanvas
@property (nonatomic,readonly) MESChartAxis *xAxis;
@property (nonatomic,readonly) MESChartAxis *yAxis;
@property (nonatomic,readonly) CAShapeLayer *xLayer;
@property (nonatomic,readonly) CAShapeLayer *yLayer;
@property (nonatomic,assign) BOOL adjustXGridToOverMidnight;
@end
