//
//  MESChartCandlesCanvas.m
//  MESChartView
//
//  Created by denis svinarchuk on 01.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartCandlesCanvas.h"

@implementation MESChartCandlesCanvas


+ (id) layer{
    id l = [[MESChartCandlesCanvas alloc] init];    
    return l;
}

- (CGFloat) widthBetween{
    if (_widthBetween==0) {
        _widthBetween = 1.0;
    }
    
    return _widthBetween;
}

- (id) init{
    self = [super init];
    
    if (self) {
        
        _widthBetween = 1.0;
        _grouthCanvas = [MESChartCanvas layer];
        _fallingCanvas = [MESChartCanvas layer];
        
        _grouthCanvas.enableGradient = NO;
        _fallingCanvas.enableGradient = NO;
        
        _fallingCanvas.fillColor = [UIColor redColor].CGColor;
        _grouthCanvas.fillColor = [UIColor greenColor].CGColor;
    }
    
    return self;
}

@end
