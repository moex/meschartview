//
//  MESChartSeries.m
//  MESChartView
//
//  Created by denis svinarchuk on 2/27/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartSeries.h"

@implementation MESChartSeries

- (id) init{
    
    self = [super init];
    
    if (self) {
        _type = MESCHART_LINE;
        _zoneIndex =  0;
        //_zoneHeight = -1;
    }
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"chart zone type: %i, index: %li", _type, (long)_zoneIndex];
}

@end


@implementation MESChartZone

- (id) init{
    self = [super init];
    if (self) {
        _height = 50.;
    }    
    return self;
}

@end