//
//  MESChartShape.m
//  MESChartView
//
//  Created by denis svinarchuk on 2/26/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MESChartCanvas.h"


static NSTimeInterval __mescanvas_animation_duration = -1.;

@implementation CAShapeLayer (MESAnimated)

+ (void) setAnimationDuration:(NSTimeInterval)duration{
    __mescanvas_animation_duration = duration;
}

- (CGFloat) animationDuration{
    if (__mescanvas_animation_duration<0) {
        return [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    }
    
    return __mescanvas_animation_duration;
}

-(void)startAnimationPath:(id)path
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    animation.duration = [self animationDuration];
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    animation.repeatCount = 0;
    animation.autoreverses = NO;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    
    animation.fromValue = (id)self.path;
    animation.toValue = (id)path;
    
    [self addAnimation:animation forKey:@"path"];
    self.path = (__bridge CGPathRef)(path);
}

- (void) setPath:(CGPathRef)__path animated:(BOOL)animated{
    if (animated){
        
        CGMutablePathRef path;
        if (__path==nil) {
            path = CGPathCreateMutable();
            CGPathMoveToPoint(path, nil, self.frame.size.width/2, self.frame.size.height);
            CGPathAddRect(path, nil, CGRectMake(self.frame.size.width/2, self.frame.size.height/2, 0, 0));
            CGPathCloseSubpath(path);
        }
        else
            path = (CGMutablePathRef)__path;
        
        [self performSelector:@selector(startAnimationPath:) withObject:(__bridge id)(path) afterDelay:.0];
        
        if (__path==nil) {
            CGPathRelease(path);
        }
    }
    else{
        [self setPath:__path];
    }
}

@end

@interface MESChartCanvasMask : CAShapeLayer
- (void) applyTo:(MESChartCanvas*)shape animate:(BOOL)animate withPath:(CGPathRef)path;
- (void) changeColor:(MESChartCanvas*)shape;
@end

@implementation MESChartCanvasMask
{
    CAGradientLayer *gradient;
    CABasicAnimation *animationPath;
}

- (id) init{
    self = [super init];
    
    if (self) {
        self.fillColor = [UIColor blackColor].CGColor;
        self.drawsAsynchronously = YES;
    }
    
    return self;
}
- (void) setShouldRasterize:(BOOL)shouldRasterize{
    [super setShouldRasterize:shouldRasterize];
    gradient.shouldRasterize = shouldRasterize;
}


- (void) changeColor:(MESChartCanvas*)shape{
    UIColor *fromColor= [UIColor colorWithCGColor:CGColorCreateCopyWithAlpha(shape.gradientColor, 0.2)];
    UIColor *midColor= [UIColor colorWithCGColor:CGColorCreateCopyWithAlpha(fromColor.CGColor, 0.1)];
    UIColor *toColor= [UIColor colorWithCGColor:CGColorCreateCopyWithAlpha(midColor.CGColor, 0.0)];
    gradient.colors = @[
                        (id) fromColor.CGColor,
                        (id) midColor.CGColor,
                        (id) toColor.CGColor
                        ];
    
    gradient.startPoint=CGPointMake(0.5, 0.0);
    gradient.endPoint=CGPointMake(0.5, 1.0);
    gradient.mask = self;
    gradient.drawsAsynchronously = YES;
    gradient.shouldRasterize = YES;
    gradient.type = kCAGradientLayerAxial;
    gradient.fillMode = kCAFillModeForwards;
}

- (void) applyTo:(MESChartCanvas *)shape animate:(BOOL)animate withPath:(CGPathRef)path{
    if (!animate) {
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    }
    
    self.frame = shape.bounds;
    if (!gradient) {
        gradient = [CAGradientLayer layer];
        
        [self changeColor:shape];
        
        [shape addSublayer:gradient];
    }
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    gradient.frame = shape.bounds;
    
    [CATransaction commit];
    
    [self setPath:path animated:animate];
    
    if (!animate) {
        [CATransaction commit];
    }
}

@end

@interface MESChartCanvas()
@property (nonatomic,strong) MESChartCanvasMask *gradientMask;
@end

@implementation MESChartCanvas
{
    CAShapeLayer *shapeOuterMask;
    CABasicAnimation *animationPath;
    CGColorRef keepFillColor;
}

@synthesize gradientColor = _gradientColor;

+ (id) layer{
    MESChartCanvas *shape = [super layer];
    return shape;
}

- (id) init{
    self = [super init];
    
    if (self) {
        
        self.fillColor= [UIColor clearColor].CGColor;
        self.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1].CGColor;
        self.lineWidth = 1.0;
        self.opacity = 1.0;
        
        self.allowsEdgeAntialiasing = YES;
        self.edgeAntialiasingMask = kCALayerBottomEdge|kCALayerLeftEdge|kCALayerRightEdge|kCALayerTopEdge;
        
        self.drawsAsynchronously = YES;
    }
    return self;
}

- (void) setGradientColor:(CGColorRef)gradientColor{
    _gradientColor = gradientColor;
    [_gradientMask changeColor:self];
}

- (CGColorRef) gradientColor{
    if (_gradientColor) {
        return _gradientColor;
    }
    
    _gradientColor = [UIColor colorWithWhite:0.5 alpha:0.5].CGColor;
    
    return _gradientColor;
}

- (MESChartCanvasMask*) gradientMask{
    if (_gradientMask) {
        return _gradientMask;
    }
    _gradientMask = [MESChartCanvasMask layer];
    return _gradientMask;
}

- (void) setShouldRasterize:(BOOL)shouldRasterize{
    [super setShouldRasterize:shouldRasterize];
    self.gradientMask.shouldRasterize = shouldRasterize;
    shapeOuterMask.shouldRasterize = shouldRasterize;
}

- (void) setNeedsDisplay{
    [super setNeedsDisplay];
    [shapeOuterMask setNeedsDisplay];
    [self.gradientMask setNeedsDisplay];
}

- (void) layoutSublayers{
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    [super layoutSublayers];
    
    if (!shapeOuterMask) {
        shapeOuterMask = [CAShapeLayer layer];
        shapeOuterMask.fillColor = [UIColor blackColor].CGColor;
        self.mask=shapeOuterMask;
        shapeOuterMask.drawsAsynchronously = YES;
    }
    
    shapeOuterMask.frame = self.bounds;
    shapeOuterMask.path = CGPathCreateWithRoundedRect(CGRectInset(self.bounds, -self.lineWidth, -self.lineWidth), 0, 0, nil);
    self.gradientMask.frame = self.bounds;
    
    [shapeOuterMask layoutSublayers];
    [self.gradientMask layoutSublayers];
    
    [CATransaction commit];
}

- (CGPathRef) path{
    return [super path];
};

- (void) setEnableGradient:(BOOL)enableGradient{
    _enableGradient = enableGradient;
    if (_enableGradient) {
        [self.gradientMask applyTo:self animate:NO withPath:self.path];
    }
    else {
        self.gradientMask.path = nil;
    }
}

- (CGColorRef) fillColor{
    return [super fillColor];
}

- (void) setPath:(CGPathRef)path{
    [super setPath:path];
    if (_enableGradient) {
        [self.gradientMask applyTo:self animate:NO withPath:path];
    }
};

- (void) setPath:(CGPathRef)path animated:(BOOL)animated{
    if (_enableGradient) {
        [self.gradientMask applyTo:self animate:YES withPath:path];
    }
    [super setPath:path animated:animated];
}

- (void) setFrame:(CGRect)frame{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [super setFrame:frame];
    [CATransaction commit];
}

@end


@interface MESChartSelectionLayer()
@property (nonatomic,strong)  CAShapeLayer *selectionFrame;
@property (nonatomic,strong)  CAShapeLayer *dashLayer;
@end

@implementation MESChartSelectionLayer
{
    CGPathRef keepingPath;
    BOOL      isActive;
    CGRect    prevFrame;
}


- (void) drawDashes{
    
    if (!isActive) {
        return;
    }
    
    if (CGRectEqualToRect(self.dashLayer.frame, prevFrame)) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGMutablePathRef path = CGPathCreateMutable();
        
        CGFloat   step = 4.0, x=-self.bounds.size.width/2, y=self.bounds.size.height;
        NSInteger lines = ceil(self.bounds.size.width/step)*2;
        
        for (NSInteger i=0; i<lines; i++, x+=step,y-=step) {
            CGPathMoveToPoint(path, nil, x, self.bounds.size.height);
            CGPathAddLineToPoint(path, nil, x+self.bounds.size.width/2., 0);
            CGPathCloseSubpath(path);
        }
        
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        
        self.dashLayer.path = path;
        self->prevFrame = self.dashLayer.frame;
        
        [CATransaction commit];
        
        CGPathRelease(path);
    });
}

- (void) layoutSublayers{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [super layoutSublayers];
    self.dashLayer.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height+100);
    [CATransaction commit];
    [self drawDashes];
}

- (void) setStrokeColor:(CGColorRef)strokeColor{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [super setStrokeColor:strokeColor];
    self.dashLayer.strokeColor = strokeColor;
    [CATransaction commit];
}

- (CAShapeLayer*) dashLayer{
    if (_dashLayer) {
        return _dashLayer;
    }
    
    _dashLayer = [CAShapeLayer layer];
    [self addSublayer:_dashLayer];
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    _dashLayer.strokeColor = [super strokeColor];
    _dashLayer.lineWidth = .5;
    _dashLayer.opacity = 0.4;
    _dashLayer.drawsAsynchronously = YES;
    
    _dashLayer.frame = self.bounds;
    [CATransaction commit];
    
    
    return _dashLayer;
}

- (CAShapeLayer*) selectionFrame{
    if (_selectionFrame) {
        return _selectionFrame;
    }
    
    _selectionFrame = [CAShapeLayer layer];
    _selectionFrame.drawsAsynchronously = YES;
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    _selectionFrame.frame = self.bounds;
    [CATransaction commit];
    
    self.mask=_selectionFrame;
    _selectionFrame.fillColor = [UIColor clearColor].CGColor;
    self.selectionFrame.path = CGPathCreateWithRoundedRect(CGRectMake(0, 0, 0, 0), 0, 0, nil);
    
    isActive = NO;
    
    return _selectionFrame;
}

- (void) setPath:(CGPathRef)path{
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    super.path = path;
    
    keepingPath = self.path;
    
    [CATransaction commit];
    
    if (!isActive) {
        self.selectionFrame.path = CGPathCreateWithRoundedRect(CGRectMake(0, 0, 0, 0), 0, 0, nil);
    }
}

- (void) unselect{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    isActive = NO;
    _selectionFrame.fillColor = [UIColor clearColor].CGColor;
    [CATransaction commit];
}

- (void) setSelection:(CGRect)selection{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        
        if (!CGColorEqualToColor( self.selectionFrame.fillColor,  [UIColor blackColor].CGColor)) {
            self.selectionFrame.fillColor = [UIColor blackColor].CGColor;
        }
        
        
        self.selectionFrame.path = CGPathCreateWithRoundedRect(CGRectMake(selection.origin.x, self.frame.origin.y, selection.size.width, self.frame.size.height), 0, 0, nil);
        
        if (!self->isActive) {
            self->isActive = YES;
            if (self.path!=self->keepingPath)
                self.path = self->keepingPath;
            [self drawDashes];
        }
        
        [CATransaction commit];
    });
}

- (void) setFrame:(CGRect)frame{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [super setFrame:frame];
    [CATransaction commit];
}

@end
