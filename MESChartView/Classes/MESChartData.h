//
//  MESChartData.h
//  MESChartView
//
//  Created by denis svinarchuk on 2/25/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    MESCHART_CANDLE_FALLING,
    MESCHART_CANDLE_GROUTH,
    MESCHART_CANDLE_FLAT
}MESChartCandelType;

@interface MESChartData : NSObject
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,strong) id xValue;
@property (nonatomic,strong) id yValue;

- (BOOL) isEqual:(id)object;
- (BOOL) isEqualxValue:(id)object;
- (BOOL) isEqualyValue:(id)object;

@end

@interface MESChartDataRange : NSObject

- (id) initWithMinimum:(id)min withMaximum:(id)max;

@property (nonatomic,readonly) id minValue;
@property (nonatomic,readonly) id maxValue;
@end

@interface MESChartOHLC : NSObject

- (id) initWithOpen:(NSNumber*)open withHigh:(NSNumber*)high withLow:(NSNumber*)low withClose:(NSNumber*)close;
- (id) initWithScalarOpen:(CGFloat)open withHigh:(CGFloat)high withLow:(CGFloat)low withClose:(CGFloat)close;

@property(nonatomic,strong) NSNumber   *open;
@property(nonatomic,strong) NSNumber   *high;
@property(nonatomic,strong) NSNumber   *low;
@property(nonatomic,strong) NSNumber   *close;

@property(nonatomic,readonly) MESChartCandelType candelType;
@property(nonatomic,readonly) NSInteger integerValue;
@property(nonatomic,readonly) CGFloat   floatValue;
@property(nonatomic,readonly) double    doubleValue;

@end