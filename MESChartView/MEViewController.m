//
//  MEViewController.m
//  MESChartView
//
//  Created by denis svinarchuk on 2/25/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "MEViewController.h"

@interface MEViewController () <MESChartDataSource, MESChartFormater, MESChartDelegate, UITabBarDelegate>
@property (nonatomic,strong) NSNumberFormatter *numberFormatter;
@property (nonatomic,strong) UIImageView *graphDeviderImageView;
@property (nonatomic,strong) UIImageView *volumeDeviderImageView;
@property (nonatomic,strong) UIImageView *leftDeviderImageView;
@property (nonatomic,strong) UIImageView *rightDeviderImageView;
@end

@implementation MEViewController
{
    NSMutableArray *data;
    NSMutableArray *volumes;
    NSMutableArray *volumes2;
    NSNumber *minYD, *maxYD;
    NSNumber *minYV, *maxYV;
    NSNumber *minYV2, *maxYV2;
}

- (void) rotateSetup:(UIInterfaceOrientation)orientation{
//    if (UIInterfaceOrientationIsLandscape(orientation)) {
//        _chart.graphLayer.lineWidth = 1.5;
//    }
//    else {
//        _chart.graphLayer.lineWidth = 1.5;
//    }
}

- (UIImageView*) graphDeviderImageView{
    if (_graphDeviderImageView) {
        return _graphDeviderImageView;
    }
    
    _graphDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xAxisDevider"]];
    [self.view addSubview:_graphDeviderImageView];
    
    return _graphDeviderImageView;
}

- (UIImageView*) volumeDeviderImageView{
    if (_volumeDeviderImageView) {
        return _volumeDeviderImageView;
    }
    
    _volumeDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xAxisDevider"]];
    [self.view addSubview:_volumeDeviderImageView];
    
    return _volumeDeviderImageView;
}

- (UIImageView*) leftDeviderImageView{
    if (_leftDeviderImageView) {
        return _leftDeviderImageView;
    }
    
    _leftDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yAxisDevider"]];
    [self.view addSubview:_leftDeviderImageView];
    
    return _leftDeviderImageView;
}

- (UIImageView*) rightDeviderImageView{
    if (_rightDeviderImageView) {
        return _rightDeviderImageView;
    }
    
    _rightDeviderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yAxisDevider"]];
    [self.view addSubview:_rightDeviderImageView];
    
    return _rightDeviderImageView;
}

- (NSDate*) dateFromString:(NSString*)date {
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return [dateFormatter dateFromString:date];
}

- (void)loadChartData:(NSString*)interval {
    data = [[NSMutableArray alloc] init];
    volumes = [[NSMutableArray alloc] init];
    volumes2 = [[NSMutableArray alloc] init];
    
    minYD = [NSNumber numberWithFloat:MAXFLOAT];
    maxYD = [NSNumber numberWithFloat:0];
    
    minYV = [NSNumber numberWithFloat:MAXFLOAT];
    maxYV = [NSNumber numberWithFloat:0];
    
    minYV2 = [NSNumber numberWithFloat:MAXFLOAT];
    maxYV2 = [NSNumber numberWithFloat:0];
    
    // load the JSON data into an array
    
    NSString *fileName = [NSString stringWithFormat:@"Candles-%@", interval];
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSData* json = [NSData dataWithContentsOfFile:filePath];
    NSArray* jdata = [NSJSONSerialization JSONObjectWithData:json
                                                    options:NSJSONReadingAllowFragments
                                                      error:nil];
    
    // iterate over the array, creating a datapoint for each value.
    int i=0;
    for (id jsonPoint  in jdata) {
        
        MESChartData *point = [[MESChartData alloc] init];
        point.index = i;
        point.xValue = [self dateFromString:[jsonPoint objectAtIndex:0]];
        MESChartOHLC *ohlc = [[MESChartOHLC alloc] 
                              initWithOpen: [[jsonPoint objectAtIndex:2] objectAtIndex:0]
                              withHigh: [[jsonPoint objectAtIndex:3] objectAtIndex:0] 
                              withLow: [[jsonPoint objectAtIndex:4] objectAtIndex:0]
                              withClose: [[jsonPoint objectAtIndex:5] objectAtIndex:0]
                              ];
        point.yValue = ohlc;
        [data addObject:point];

        if (minYD.floatValue>[ohlc.low floatValue]) {
            minYD = [NSNumber numberWithFloat:[ohlc.low floatValue]];
        }
        if (maxYD.floatValue<[ohlc.high floatValue]) {
            maxYD = [NSNumber numberWithFloat:[ohlc.high floatValue]];
        }
        
        MESChartData *point2 = [[MESChartData alloc] init];
        point2.index = i;
        point2.xValue = point.xValue;        
        point2.yValue = [[jsonPoint objectAtIndex:6] objectAtIndex:0];        
        [volumes addObject:point2];

        NSNumber *y2 = point2.yValue;
        if (minYV.floatValue>y2.floatValue) {
            minYV = point2.yValue;
        }
        if (maxYV.floatValue<y2.floatValue) {
            maxYV = point2.yValue;
        }
        
        CGFloat v2 = i*i/10;
        MESChartData *point3 = [[MESChartData alloc] init];
        point3.index = i;
        point3.xValue = point.xValue;        
        point3.yValue = [NSNumber numberWithFloat:v2];        
        [volumes2 addObject:point3];

        if (minYV2.floatValue>v2) {
            minYV2 = [NSNumber numberWithFloat:v2];
        }
        if (maxYV2.floatValue<v2) {
            maxYV2 = [NSNumber numberWithFloat:v2];
        }        

        i++;
        if (i>5) {
            //break;
        }
    }
}

- (void) tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSLog(@"ITEM BAR: %@: %@", item, item.title);
    [self loadChartData:item.title];
    [self redraw:[UIApplication sharedApplication].statusBarOrientation];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _chart.dataSource = self;
    _chart.formater = self;
    _chart.delegate = self;
    
    [self redraw:[UIApplication sharedApplication].statusBarOrientation];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.tabBar.delegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) loadView{
    [super loadView];
    
    [self loadChartData:@"M10"];

    [MESChartView setAnimationDuration:0.2];
    
    _chart = [[MESChartView alloc] initWithFrame:CGRectInset(self.view.bounds, 0., 0.)];
    _chart.graphLayer.lineWidth = 1.5;

    _chart.backgroundColor = [UIColor whiteColor];
    _chart.gridLayer.yAxis.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:10];
    _chart.gridLayer.yAxis.textColor = [UIColor darkGrayColor];
    _chart.gridLayer.yAxis.ticks = 8;
    _chart.gridLayer.yAxis.enableEdges = YES;
    _chart.gridLayer.yAxis.enableLabels = YES;

    _chart.gridLayer.xAxis.width = 0.0;
    _chart.gridLayer.lineWidth = 0.3;
    _chart.gridLayer.yLayer.lineWidth = 0.2;
    _chart.gridLayer.xLayer.lineWidth = 0.3;
    _chart.gridLayer.xAxis.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11];
    _chart.gridLayer.xAxis.textColor = [UIColor grayColor];
    _chart.gridLayer.xAxis.ticks = 6;
    _chart.gridLayer.xAxis.enableLabels = NO;
    _chart.gridLayer.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4].CGColor;
    _chart.gridLayer.yLayer.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2].CGColor;
    _chart.gridLayer.adjustXGridToOverMidnight = YES;
    
    _chart.crossHairLayer.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6].CGColor;
    _chart.crossHairLayer.lineWidth = 0.5;
    
    _chart.graphLayer.gradientColor = [UIColor colorWithRed:0.7 green:0.1 blue:0.1 alpha:0.3].CGColor;
    _chart.graphLayer.strokeColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:.5].CGColor;
    _chart.graphLayer.enableGradient = YES;
    
    _chart.candlesLayer.grouthCanvas.fillColor = [UIColor whiteColor].CGColor;
    _chart.candlesLayer.fallingCanvas.fillColor = [UIColor colorWithRed:98./255. green:164./255 blue:209./255 alpha:1.].CGColor;
    _chart.candlesLayer.grouthCanvas.strokeColor = _chart.candlesLayer.fallingCanvas.strokeColor = _chart.graphLayer.strokeColor = _chart.graphLayer.gradientColor = _chart.candlesLayer.fallingCanvas.fillColor;
    

    _chart.selectionLayer.fillColor =  _chart.candlesLayer.fallingCanvas.fillColor;//[UIColor redColor].CGColor;
    _chart.selectionLayer.strokeColor = [UIColor blueColor].CGColor;
    _chart.selectionLayer.opacity = 0.3;
    
    _chart.candlesLayer.grouthCanvas.lineWidth = 0.5;
    _chart.candlesLayer.fallingCanvas.lineWidth = 0.5;
    _chart.candlesLayer.widthBetween = 1;
    _chart.headerView.backgroundColor = [UIColor colorWithCGColor:_chart.candlesLayer.grouthCanvas.fillColor];
    _chart.titleView.backgroundColor = [UIColor clearColor];
    _chart.titleView.textLabel.text = [NSString stringWithFormat:@"Sberbank"];

    _chart.zonePadding = 1.0;
    _chart.edgePadding = 5.0;
    
    [self.containerView addSubview:_chart];
    _chart.translatesAutoresizingMaskIntoConstraints = NO;
    
    _chart.backgroundImage = [UIImage imageNamed:@"gridBackground" ];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[chart]-5-|" options:0 metrics:nil views:@{@"chart":_chart}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-25-[chart]-5-|" options:0 metrics:nil views:@{@"chart":_chart, @"bottom": self.bottomLayoutGuide}]];    

    [self rotateSetup:[UIApplication sharedApplication].statusBarOrientation];

}

- (void) redraw:(UIInterfaceOrientation)toInterfaceOrientation{
        CGMutablePathRef path = nil;
        if (UIInterfaceOrientationIsLandscape(/*[UIApplication sharedApplication].statusBarOrientation*/toInterfaceOrientation)) {
            [_chart.graphLayer setPath:path animated:YES];
        }
        else {
            [_chart.candlesLayer.grouthCanvas setPath:path animated:YES];
            [_chart.candlesLayer.fallingCanvas setPath:path animated:YES];
        }
        [self rotateSetup:toInterfaceOrientation];
            
    dispatch_async(dispatch_get_main_queue(), ^{
        [_chart redraw];
        
        CGRect rect =CGRectMake(_chart.rootLayer.frame.origin.x, _chart.rootLayer.frame.origin.y+_chart.graphLayer.bounds.size.height+25, _chart.graphLayer.bounds.size.width, 1);
        self.graphDeviderImageView.frame = rect;
        rect.origin.y +=_chart.zonePadding*2+[self volumeZoneHeight]-10.;
        self.volumeDeviderImageView.frame = rect;
        
        rect =CGRectMake(_chart.rootLayer.frame.origin.x+3, _chart.rootLayer.frame.origin.y+[self volumeZoneHeight], 1, _chart.graphLayer.bounds.size.height);
        self.leftDeviderImageView.frame = rect;
        
        rect.origin.x += _chart.graphLayer.bounds.size.width+3;
        self.rightDeviderImageView.frame = rect;

    });
}


- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self redraw:[UIApplication sharedApplication].statusBarOrientation];
}

- (MESChartSeries*) meSChartView:(MESChartView *)chart seriesAtIndex:(NSInteger)index{
    MESChartSeries *series = [[MESChartSeries alloc] init];
    if (index == 0) {
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            series.type = MESCHART_CANDLE;
        }
        else{
            series.type = MESCHART_LINE;
        }
        series.zoneIndex = 0;
    }
    else if (index==1) {
        series.type = MESCHART_BAR;
        series.zoneIndex = 1;
    }
    else if (index==2) {
        series.type = MESCHART_BAR;
        series.zoneIndex = 2;
    }
    return series;
}

- (NSInteger) numberOfSeriesInSChartView:(MESChartView *)chart{
    return 2;
}

static int __shift=0;

- (NSInteger) meSChartView:(MESChartView *)chart numberOfChartDataForSeriesAtIndex:(NSInteger)index{
    
    NSInteger shift = 0;
    if (!UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        shift=__shift;
    }

    if (index==0) {
        return data.count-shift;
    }
    else if (index == 1)
        return volumes.count-shift;
    
    return volumes2.count-shift;
}


- (MESChartData*) meSChartView:(MESChartView *)chart chartDataAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    NSInteger index = dataIndex;
    if (!UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        index = dataIndex+__shift;
    }
    
    if (seriesIndex==0) {
        return [data objectAtIndex:index];
    }
    else if (seriesIndex==1)
        return [volumes objectAtIndex:index];
    else
        return [volumes2 objectAtIndex:index];        
}

- (MESChartDataRange*) dataRangeInSChartView:(MESChartView *)chart atSeriesIndex:(NSInteger)index{
    if (index==0) {
      return [[MESChartDataRange alloc] initWithMinimum:minYD withMaximum:maxYD];
    }
    else if (index==1)
        return [[MESChartDataRange alloc] initWithMinimum:minYV withMaximum:maxYV];
    
    return [[MESChartDataRange alloc] initWithMinimum:minYV2 withMaximum:maxYV2];
}

- (NSNumberFormatter*) numberFormatter{
    if (!_numberFormatter) {
        
        _numberFormatter = [[NSNumberFormatter alloc] init];
        
        [_numberFormatter allowsFloats];
        [_numberFormatter setLocale:[NSLocale currentLocale]];
        [_numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [_numberFormatter setAlwaysShowsDecimalSeparator:NO];
        [_numberFormatter setMinimumSignificantDigits:0];
        
        [_numberFormatter setMaximumSignificantDigits:2];
        [_numberFormatter setMaximumFractionDigits:2];
        [_numberFormatter setMinimumFractionDigits:2];
    }
    
    return _numberFormatter;
}

- (void) meSChartView:(MESChartView *)chart yAxisLabel:(UILabel *)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    if (seriesIndex>0) {
        label.text = [self.numberFormatter stringFromNumber:value];
        CGSize size = label.bounds.size;
        size = [label sizeThatFits:CGSizeMake(_chart.gridLayer.bounds.size.width, label.bounds.size.height)];
        label.frame = CGRectMake(_chart.gridLayer.bounds.size.width - size.width + self.chart.gridLayer.yAxis.labelPadding+self.chart.edgePadding, label.frame.origin.y, size.width, label.bounds.size.height);
        label.backgroundColor = [UIColor colorWithWhite:1 alpha:0.6];
        return;
    }
    label.text = [self.numberFormatter stringFromNumber:value];
}


#pragma mark - formaters
//
//
//
- (void) meSChartView:(MESChartView *)chart xAxisLabel:(UILabel *)label withValue:(id)value atTickIndex:(NSInteger)tickIndex forSeriesAtIndex:(NSInteger)seriesIndex{
    NSDate *time=value;
    label.text = [NSString stringWithFormat:@"%@", [NSDateFormatter localizedStringFromDate:time dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle]];
    //NSLog(@"LABEL (%@): index=%li tickIndex=%li  -> %@   ishidden=%i alpha=%f", label ,label.tag, tickIndex, label.text, label.isHidden, label.alpha);
}

- (void) meSChartView:(MESChartView *)chart headerView:(UILabel *)label atLeftFingerData:(MESChartData *)leftData atRightFingerData:(MESChartData *)rightData{
    NSNumber *value;
    if (leftData && rightData) {
        CGFloat trend = 0.;
        NSNumber *numberTrend;
        if ([leftData.yValue isKindOfClass:[MESChartOHLC class]]) {
            MESChartOHLC *ohlc_left = leftData.yValue;
            MESChartOHLC *ohlc_right = rightData.yValue;
            trend = ohlc_right.floatValue - ohlc_left.floatValue;
            value = [NSNumber numberWithFloat:ohlc_left.floatValue];
        }
        else{
            value = leftData.yValue;
        }
        
        if (trend>0) {
            label.backgroundColor = [UIColor colorWithRed:0.1 green:0.8 blue:0.01 alpha:0.7];
        }
        else if (trend<0){
            label.backgroundColor = [UIColor colorWithRed:0.8 green:0.1 blue:0.01 alpha:0.7];
        }
        else{
            label.backgroundColor = [UIColor colorWithRed:0.01 green:0.1 blue:0.8 alpha:0.7];
        }
        
        numberTrend = [NSNumber numberWithFloat:trend];
        NSNumber *trendInPerc = [NSNumber numberWithFloat: (trend/value.floatValue)*100.];
        
        label.text = [NSString stringWithFormat:@"%@: %@ (%@%%)", [self.numberFormatter stringFromNumber:value], [self.numberFormatter stringFromNumber:numberTrend], [self.numberFormatter stringFromNumber:trendInPerc]];
    }
    else if (leftData){
        if ([leftData.yValue isKindOfClass:[MESChartOHLC class]]) {
            MESChartOHLC *ohlc_left = leftData.yValue;
            value = [NSNumber numberWithFloat:ohlc_left.floatValue];
        }
        else{
            value = leftData.yValue;
        }
        label.backgroundColor = [UIColor colorWithRed:0.01 green:0.1 blue:0.8 alpha:0.7];

        NSDate *lefttime = leftData.xValue;
        label.text =  [NSString stringWithFormat:@"%@: %@", [NSDateFormatter localizedStringFromDate:lefttime dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle], [self.numberFormatter stringFromNumber:value]];
    }
}

- (NSInteger) numberOfZonesInSChartView:(MESChartView *)chart{
    return 1;
}

- (CGFloat) volumeZoneHeight{
    CGFloat height = 0.0;
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            height = 100.;
        }
        else{
            height = 30.;
        }
    }
    else{
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            height = 200.;
        }
        else{
            height = 50.;
        }
    }
    return height;
}

- (MESChartZone*) meSChartView:(MESChartView *)chart zoneAtIndex:(NSInteger)zoneIndex{
    MESChartZone *zone = [[MESChartZone alloc] init];
    zone.height = [self volumeZoneHeight];
    if (zoneIndex==0) {
        zone.grid = [MESChartGrid layer];
        
        zone.grid.adjustXGridToOverMidnight = YES;
        
        zone.grid.xAxis.width = 10.0;
        zone.grid.xLayer.lineWidth = _chart.gridLayer.xLayer.lineWidth;
        zone.grid.yLayer.lineWidth = 0.0;
        zone.grid.yAxis.font = _chart.gridLayer.yAxis.font;
        zone.grid.yAxis.textColor = _chart.gridLayer.yAxis.textColor;
        zone.grid.xAxis.font = _chart.gridLayer.xAxis.font;
        zone.grid.xAxis.textColor = _chart.gridLayer.xAxis.textColor;
        zone.grid.xAxis.ticks = _chart.gridLayer.xAxis.ticks;
        zone.grid.yAxis.enableLabels = YES;
    }
    return zone;
}

#pragma mark - delegate

- (void) meSChartView:(MESChartView *)chart beganTouchWithPoints:(NSArray *)points withDataIndex:(NSArray *)indxs atSeriesindex:(NSInteger)seriesIndex{
    NSLog(@"Began touch: %@ %@", points, indxs);
}

- (void) meSChartView:(MESChartView *)chart finishedTouchWithPoints:(NSArray *)points withDataIndex:(NSArray *)indxs atSeriesindex:(NSInteger)seriesIndex{
    NSLog(@"Finished touch: %@ %@", points, indxs);
}

- (void) fooLoad{
    data = [[NSMutableArray alloc] init];
    volumes = [[NSMutableArray alloc] init];
    volumes2 = [[NSMutableArray alloc] init];
    
    minYD = [NSNumber numberWithFloat:MAXFLOAT];
    maxYD = [NSNumber numberWithFloat:0];
    
    minYV = [NSNumber numberWithFloat:MAXFLOAT];
    maxYV = [NSNumber numberWithFloat:0];
    
    minYV2 = [NSNumber numberWithFloat:MAXFLOAT];
    maxYV2 = [NSNumber numberWithFloat:0];
    
    for (int i=0; i<50; i++) {
        //CGFloat y= fabs((tanf(i*i*i)+1)*10*i+360);
        CGFloat y= abs(i*(exp2(i/10)));
        //CGFloat y= (tanf(i*i*i)+1)*10*i+360;
        //CGFloat y = i*i*tan(sin(i))/1000;
        CGFloat v = 100+arc4random();
        CGFloat v2 = i*i/10;
        
        MESChartData *point = [[MESChartData alloc] init];
        point.index = i;
        point.xValue = [NSNumber numberWithInt:i];        
        CGFloat alter = i%2==0?-1.:1.;
        MESChartOHLC *ohlc = [[MESChartOHLC alloc] initWithScalarOpen:y+alter*40 withHigh:y+60 withLow:y withClose:y+30.0];
        point.yValue = ohlc;
        [data addObject:point];
        
        MESChartData *point2 = [[MESChartData alloc] init];
        point2.index = i;
        point2.xValue = [NSNumber numberWithInt:i];        
        point2.yValue = [NSNumber numberWithFloat:v];        
        [volumes addObject:point2];
        
        MESChartData *point3 = [[MESChartData alloc] init];
        point3.index = i;
        point3.xValue = [NSNumber numberWithInt:i];        
        point3.yValue = [NSNumber numberWithFloat:v2];        
        [volumes2 addObject:point3];
        
        if (minYD.floatValue>y) {
            minYD = [NSNumber numberWithFloat:[ohlc.low floatValue]];
        }
        if (maxYD.floatValue<y) {
            maxYD = [NSNumber numberWithFloat:[ohlc.high floatValue]];
        }
        
        if (minYV.floatValue>v) {
            minYV = [NSNumber numberWithFloat:v];
        }
        if (maxYV.floatValue<v) {
            maxYV = [NSNumber numberWithFloat:v];
        }
        
        if (minYV2.floatValue>v2) {
            minYV2 = [NSNumber numberWithFloat:v2];
        }
        if (maxYV2.floatValue<v2) {
            maxYV2 = [NSNumber numberWithFloat:v2];
        }        
    }
    
}

@end
