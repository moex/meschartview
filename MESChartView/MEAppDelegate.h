//
//  MEAppDelegate.h
//  MESChartView
//
//  Created by denis svinarchuk on 2/25/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
