# MESChartView

Created by **denn.nevera**

MESChartView is a Cocoa framework for OSX/iOS which provides high level api to draw stock charts on UIView.

# How to use

* Install cocoapods: http://cocoapods.org/
* Edit the Podfile in your XCode project directory YourApp:

```
#!bash
    $ edit Podfile
    platform :ios, '7.0'
    pod 'MESChartView', :git => 'https://bitbucket.org/denn_nevera/meschartview.git', :tag => '0.1.0'
```

* Install dependences in your project

```
#!bash
    $ pod install
```

* Open the XCode workspace instead of the project file

```
#!bash
    $ open YourApp.xcworkspace
```

* Import API

```
#!objective-c

    #import "MESChartView.h"

```

# Example

```
#!objective-c

#import <Foundation/Foundation.h>
#import "MESChartView.h"
...

```

